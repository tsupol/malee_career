# Malee Responsive HTML + CSS

## How to use

All you need for the front-end is in the `app` folder.
Other files outside is for `development` only.

### Note

The scope of this project is HTML + CSS only and not include the followings...

- The connection with API
- Server configurations
- The business logics (e.g. form validation, displaying error message)
- The actual links and texts (not from the designs).



