<?php require_once("components/header.php"); ?>
<div class="ml-page page-profile">
  <div class="sec-banner-2"></div>
  <div class="layout-outer">
    <div class="layout-inner">
      <div class="layout-profile-page">

        <?php
        require_once('components/profile_nav.php');
        get_profile_nav(4)
        ?>

        <div class="sec-content">
          <h1 class="heading1">การตั้งค่า</h1>
          <!-- Card 1-->
          <div class="card-profile">
            <div class="cp-inner">
              <h1>ข้อมูลการเข้าสู่ระบบ</h1>
              <div class="profile-settings">
                <div class="__row">
                  <div class="__col">บัญชีผู้ใช้:</div>
                  <div class="__col">mail@mail.com</div>
                </div>
                <div class="__row">
                  <div class="__col">รหัสผ่าน:</div>
                  <div class="__col">**********
                    <a class="ml-link" href="profile-password.php">[เปลี่ยนรหัสผ่าน]</a></div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $(function () {
    $('#theForm').validate();
  });
</script>

<?php require_once("components/footer.php"); ?>
