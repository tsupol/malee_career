<?php
$LOGGED_IN = true;
require_once("components/header.php");
?>
<style>
  li {
    line-height: 24px;
    font-weight: bold;
    color: #333333;
  }
  a {
    color: #4c56ff;
    cursor: pointer;
    font-weight: 600;
  }
  p {
    font-weight: normal;
  }
  strong {
    color: #df2c2f;
  }
</style>
<div class="ml-page page-login">
  <div class="layout-outer">
    <div class="layout-inner">
      <h1>Front-end Sitemap</h1>
      <h3>Pages</h3>
      <ul>
        <li><a href="index.php" target="_blank" target="_blank">Home</a></li>
        <li><a href="work-with-us.php" target="_blank">Work With Us (Simulate user logged-in here)</a>
          <ul>
            <li><a href="job-description.php" target="_blank">Job Description</a></li>
            <li><a href="apply-job.php" target="_blank">Apply Job</a></li>
            <li><a href="apply-job-info.php" target="_blank">Apply Job Info</a></li>
          </ul>
        </li>
        <li><a href="activity.php" target="_blank">Activities</a>
          <ul>
            <li><a href="activity-content.php" target="_blank">Activity Content</a></li>
          </ul>
        </li>
        <li><a href="contact.php" target="_blank">Contact HR</a></li>
        <li>Authentications
          <ul>
            <li><a href="login.php" target="_blank">Login</a></li>
            <li><a href="register.php" target="_blank">Register</a></li>
            <li><a href="thank-you.php" target="_blank">Thank You for Registering</a></li>
            <li><a href="forgot-password.php" target="_blank">Forgot Password</a></li>
            <li><a href="change-password.php" target="_blank">Change Password</a></li>
          </ul>
        </li>
        <li><a href="terms-conditions.php" target="_blank">Terms and Conditions</a></li>
        <li>Profile
          <ul>
            <li><a href="profile.php" target="_blank">Profile Overview</a></li>
            <li><a href="profile-history.php" target="_blank">Profile Work History</a></li>
            <li><a href="profile-settings.php" target="_blank">Profile Settings</a></li>
            <li><a href="profile-password.php" target="_blank">Profile Change Password</a></li>
            <li>Profile Creation (Steps)
              <ul>
                <li><a href="register-profile_1.php" target="_blank">Step 1</a></li>
                <li><a href="register-profile_2.php" target="_blank">Step 2</a></li>
                <li><a href="register-profile_3.php" target="_blank">Step 3</a></li>
                <li><a href="register-profile_4.php" target="_blank">Step 4</a></li>
                <li><a href="register-profile_5.php" target="_blank">Step 5</a></li>
                <li><a href="register-profile_6.php" target="_blank">Step 6</a></li>
                <li><a href="register-profile_7.php" target="_blank">Step 7</a></li>
                <li><a href="register-profile_8.php" target="_blank">Step 8</a></li>
              </ul>
            </li>
            <li>Profile Edit (steps)
              <ul>
                <li><a href="profile-edit.php" target="_blank">Step 1</a></li>
                <li><a href="profile-edit_2.php" target="_blank">Step 2</a></li>
                <li><a href="profile-edit_3.php" target="_blank">Step 3</a></li>
                <li><a href="profile-edit_4.php" target="_blank">Step 4</a></li>
                <li><a href="profile-edit_5.php" target="_blank">Step 5</a></li>
                <li><a href="profile-edit_6.php" target="_blank">Step 6</a></li>
                <li><a href="profile-edit_7.php" target="_blank">Step 7</a></li>
                <li><a href="profile-edit_8.php" target="_blank">Step 8</a></li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>

      <h3>Popups</h3>
      <ul>
        <li>
          <a class="pop-subscribe">Subscribe</a>
            <p>Note: Click the <strong>Subscribe</strong> button to activate the <strong>Newsletters Popup.</strong></p>
        </li>
        <li><a class="pop-share">Share</a></li>
        <li><a class="pop-thank-register">Thank you (register profile)</a></li>
      </ul>

      <h3>Disclaimers</h3>
      <ul>
        <li>The scope of this project is actually <strong>Responsive HTML + CSS only.</strong></li>
        <li>The javascript is only included as necessary.</li>
        <li>This project only provides mocking interface <strong>not the actual one.</strong></li>
        <li><strong>The implementation</strong> is not in the scope.</li>
        <li>I am willing to provide assistance for the implementation part <strong>but that is not my responsibility.</strong></li>
        <li>The implementation includes the following...
          <ul>
            <li>API connections or anything related to server</li>
            <li>Business Logic (e.g. uploading file/profile image, fully functional form, etc.)</li>
            <li>The actual navigation (URL links)</li>
          </ul>
        </li>
      </ul>

    </div>
  </div>
</div>

<script>
  $(function () {
    $('.main-menu, .sec-subscribe').css('display', 'none')
  });
</script>


<?php require_once("components/footer.php"); ?>
