<?php require_once("components/header.php"); ?>
<div class="ml-page page-profile">
  <div class="sec-banner-2"></div>
  <div class="layout-outer">
    <div class="layout-inner">
      <div class="layout-profile-page">

        <?php
        require_once('components/profile_nav.php');
        get_profile_nav(4)
        ?>

        <div class="sec-content">
          <h1 class="heading1">การตั้งค่า</h1>
          <!-- Card 1-->
          <div class="card-profile">
            <div class="cp-inner">
              <h1>เปลี่ยนรหัสผ่าน</h1>
              <div class="profile-password">
                <form id="theForm" class="ml-form login-form" action="./" method="POST">
                  <div class="clearfix">
                    <div class="sm-col col-12 px2">
                      <div class="form-item">
                        <input class="ml-input" name="password" id="old_password" type="password">
                        <label class="label" for="old_password">กรอกรหัสผ่านเก่า</label>
                        <label class="caption" for="old_password">กรุณากรอกรหัสผ่านมากกว่า 6 ตัวอักษรss</label>
                      </div>
                    </div>
                    <div class="sm-col col-12 px2">
                      <div class="form-item">
                        <input class="ml-input" name="password" type="password" id="password">
                        <label class="label" for="password">กรอกรหัสผ่านใหม่</label>
                        <label class="caption" for="password">กรุณากรอกรหัสผ่านมากกว่า 6 ตัวอักษร</label>
                      </div>
                    </div>
                    <div class="sm-col col-12 px2">
                      <div class="form-item">
                        <input class="ml-input" name="confirm_password" type="password">
                        <label class="label" for="confirm_password">ยืนยันรหัสผ่านใหม่</label>
                      </div>
                    </div>
                    <div class="sm-col col-12 px2 buttons">
                      <button type="submit" class="btn">ยืนยัน</button>
                      <a href="profile-settings" class="btn primary">บันทึก</a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

<script>

  $(function () {
    $('#theForm').validate({
      rules: {
        password: {
          required: true,
          minlength: 6,
        },
        confirm_password: {
          equalTo: '#password',
        }
      },
      messages: {
        password: {
          required: 'กรุณากรอกรหัสผ่านมากกว่า 6 ตัวอักษร',
          minlength: 'กรุณากรอกรหัสผ่านมากกว่า 6 ตัวอักษร',
        },
        confirm_password: {
          equalTo: 'รหัสผ่านไม่ตรงกัน',
        }
      },
    });
  });
</script>

<?php require_once("components/footer.php"); ?>
