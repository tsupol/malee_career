<?php
require_once("components/header.php");
require_once("components/social_share.php");
?>
<div class="ml-page page-activity ml-normalize">
  <div class="layout-outer">
    <div class="layout-inner">
      <ul class="breadcrumb">
        <li><a href="index.php">หน้าแรก</a></li>
        <li><a href="work-with-us.php">ร่วมงานกับมาลี</a></li>
        <li><a class="active">โครงการประกอบการพบนักศึกษา...</a></li>
      </ul>

      <div class="content-block">
        <div class="_content pr3">
          <h6 class="__super">กิจกรรมครอบครัวมาลี</h6>
          <h1 class="__title">โครงการประกอบการพบนักศึกษา / คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง</h1>
          <h6 class="__date">4 Sep 2018</h6>
          <p class="__desc">
            หากคุณเป็นคนรุ่นใหม่ มีไฟที่จะเรียนรู้ทักษะต่างๆ กระตือรือร้นในการทำงานกับทีมขนาดเล็ก ที่มีความอิสระสูงในการทำงาน เป็นโปรแกรมเมอร์ที่ต้องการเพิ่มพูนความรู้ในเชิงธุรกิจ (ERP)
            หรือชื่นชอบในความเป็น Open Source + Sharing และพร้อมที่จะเติบโตไปพร้อมกัน ตำแหน่ง งานนี้เหมาะกับคุณ ทั้งนี้ไม่ใช่ครั้งแรกที่มีเหตุการณ์แบบนี้เกิดขึ้น โดยก่อนหน้านี้ ในเดือนเมษายน
            นักท่องเที่ยวชาวเยอรมัน 2 คน ถูกเสือดาวพุ่งเข้าโจมตี ขณะที่พวกเขากำลัง ตั้งแคมป์กันอยู่ใน กุยเซ็บแคนยอน ประเทศนามิเบีย เสือดาวดังกล่าวกระโจนเข้ามาในรถ และงับศีรษะพวกเขา
            แต่เคราะห์ดีที่มันผละออกไปหลังจากนั้น พวกเขาจึงรอดชีวิตมาได้ สำหรับกรณีของเด็กชายวัย
            3 ขวบ นั้น บาเชียร์ ฮังกี โฆษกอุทยานแห่งชาติควีนเอลิซาเบธ เปิดเผยว่า เจ้าหน้าที่อุทยานกำลัง ระดมกำลังค้นหาเสือดาวตัวดังกล่าว ตอนนี้มันเป็นสัตว์ที่อันตรายมาก เนื่องจากมันเคยได้ลิ้มรส
            เนื้อมนุษย์มาแล้ว มันจะอยากกินอีก เจ้าหน้าที่จึงจำเป็นต้องกำจัดมันทิ้ง เพื่อป้องกันไม่ให้มันออกล่ามนุษย์มากินในอนาคต
          </p>
        </div>
        <div class="_img">
          <img src="<?php echo $asset_path ?>imgs/activity/act-1.jpg"/>
        </div>
      </div>

      <div class="sec-banner"></div>

      <div class="content-block">
        <div class="_img pl2 pr3">
          <img src="<?php echo $asset_path ?>imgs/activity/act-8.jpg"/>
        </div>
        <div class="_content">
          <p class="__desc">
            หากคุณเป็นคนรุ่นใหม่ มีไฟที่จะเรียนรู้ทักษะต่างๆ กระตือรือร้นในการทำงานกับทีมขนาดเล็กที่มีความอิสระสูง ในการทำงาน เป็นโปรแกรมเมอร์ที่ต้องการเพิ่มพูนความรู้ในเชิงธุรกิจ (ERP)
            หรือชื่นชอบในความเป็น Open Source + Sharing และพร้อมที่จะเติบโตไปพร้อมกัน ตำแหน่งงานนี้เหมาะกับคุณ
            <br/><br/>
            ทั้งนี้ไม่ใช่ครั้งแรกที่มีเหตุการณ์แบบนี้เกิดขึ้น โดยก่อนหน้านี้ในเดือนเมษายน นักท่องเที่ยวชาวเยอรมัน
            2 คน ถูกเสือดาวพุ่งเข้าโจมตี ขณะที่พวกเขากำลังตั้งแคมป์กันอยู่ในกุยเซ็บแคนยอน ประเทศนามิเบีย เสือดาว ดังกล่าวกระโจนเข้ามาในรถและงับศีรษะพวกเขา แต่เคราะห์ดีที่มันผละออกไปหลังจากนั้น
            พวกเขาจึงรอดชีวิต
            <br/><br/>
            สำหรับกรณีของเด็กชายวัย 3 ขวบ นั้น บาเชียร์ ฮังกี โฆษกอุทยานแห่งชาติควีนเอลิซาเบธ เปิดเผยว่า เจ้าหน้าที่อุทยานกำลังระดมกำลังค้นหาเสือดาวตัวดังกล่าว ตอนนี้มันเป็นสัตว์ที่อันตรายมาก
            เนื่องจากมันเคย ได้ลิ้มรสเนื้อมนุษย์มาแล้ว มันจะอยากกินอีก เจ้าหน้าที่จึงจำเป็นต้องกำจัดมันทิ้ง เพื่อป้องกันไม่ให้มันออกล่า มนุษย์มากินในอนาคต แต่อย่างไรก็ตาม
            ไม่มีการรายงานเพิ่มเติมว่าเจ้าหน้าที่พบเจอเสือดาวตัวดังกล่าว
            <br/><br/>
            หากคุณเป็นคนรุ่นใหม่ มีไฟที่จะเรียนรู้ทักษะต่างๆ กระตือรือร้นในการทำงานกับทีมขนาดเล็กที่มีความอิสระสูง ในการทำงาน เป็นโปรแกรมเมอร์ที่ต้องการเพิ่มพูนความรู้ในเชิงธุรกิจ (ERP)
            หรือชื่นชอบในความเป็น Open Source + Sharing และพร้อมที่จะเติบโตไปพร้อมกัน ตำแหน่งงานนี้เหมาะกับคุณ
          </p>
        </div>
      </div>

      <!-- Gallery -->
      <div class="content-gallery">
        <a href="#" class="_img" style="background-image: url('<?php echo $asset_path ?>imgs/activity/act-3.jpg')"></a>
        <a href="#" class="_img" style="background-image: url('<?php echo $asset_path ?>imgs/activity/act-4.jpg')"></a>
        <a href="#" class="_img" style="background-image: url('<?php echo $asset_path ?>imgs/activity/act-5.jpg')"></a>
        <a href="#" class="_img" style="background-image: url('<?php echo $asset_path ?>imgs/activity/act-6.jpg')">
          <div class="__count">+9</div>
        </a>
      </div>

      <!-- Tags -->
      <h3 class="heading-tag">Tags</h3>
      <ul class="tags">
        <li class="_tag"><a href="#">บำรุงสายตา</a></li>
        <li class="_tag"><a href="#">บำรุงผิวพรรณ</a></li>
        <li class="_tag"><a href="#">บำรุงประสาทและสมอง</a></li>
        <li class="_tag"><a href="#">เสริมสร้างสุขภาพกาย</a></li>
        <li class="_tag"><a href="#">องค์การอนามัยโลก</a></li>
      </ul>

      <div class="act-back font2 flex-center">
        <a href="#" class="btn-underscore">BACK</a>
      </div>

      <!-- Related -->
      <h2 class="heading2">กิจกรรมที่เกี่ยวข้อง</h2>

      <div class="act-sec act-sec-list related-act">
        <div class="act-item col-12 xs-col-6 md-col-3">
          <div class="_padding">
            <div class="_inner">
              <a href="activity-content.php" class="_img">
                <img src="<?php echo $asset_path ?>imgs/activity/act-3.jpg"/>
              </a>
              <div class="_content">
                <p class="__title">
                  <a href="activity-content.php">คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอม เกล้าเจ้าคุณทหารลาดกระบัง</a>
                </p>
                <p class="__link"><a href="#">อ่านต่อ</a></p>
              </div>
            </div>
          </div>
        </div>
        <div class="act-item col-12 xs-col-6 md-col-3">
          <div class="_padding">
            <div class="_inner">
              <a href="activity-content.php" class="_img">
                <img src="<?php echo $asset_path ?>imgs/activity/act-4.jpg"/>
              </a>
              <div class="_content">
                <p class="__title">
                  <a href="activity-content.php">คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอม เกล้าเจ้าคุณทหารลาดกระบัง</a>
                </p>
                <p class="__link"><a href="#">อ่านต่อ</a></p>
              </div>
            </div>
          </div>
        </div>
        <div class="act-item col-12 xs-col-6 md-col-3">
          <div class="_padding">
            <div class="_inner">
              <a href="activity-content.php" class="_img">
                <img src="<?php echo $asset_path ?>imgs/activity/act-5.jpg"/>
              </a>
              <div class="_content">
                <p class="__title">
                  <a href="activity-content.php">คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอม เกล้าเจ้าคุณทหารลาดกระบัง</a>
                </p>
                <p class="__link"><a href="#">อ่านต่อ</a></p>
              </div>
            </div>
          </div>
        </div>
        <div class="act-item col-12 xs-col-6 md-col-3">
          <div class="_padding">
            <div class="_inner">
              <a href="activity-content.php" class="_img">
                <img src="<?php echo $asset_path ?>imgs/activity/act-6.jpg"/>
              </a>
              <div class="_content">
                <p class="__title">
                  <a href="activity-content.php">คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอม เกล้าเจ้าคุณทหารลาดกระบัง</a>
                </p>
                <p class="__link"><a href="#">อ่านต่อ</a></p>
              </div>
            </div>
          </div>
        </div>

      </div>


    </div>
  </div>
</div>

<script>
  $(function () {
    $('#theForm').validate();
  });
</script>

<?php require_once("components/footer.php"); ?>
