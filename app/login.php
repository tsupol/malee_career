<?php require_once("components/header.php"); ?>
<div class="ml-page page-login">
  <div class="layout-outer">
    <div class="layout-inner flex-center">
      <div class="layout-narrow pad-top-1">

        <div class="clearfix">
          <div class="sm-col col-12 px2">
            <button class="btn btn-facebook hover-fade"><i class="fab fa-facebook-f"></i>เข้าสู่ระบบด้วย Facebook</button>
          </div>
          <div class="sm-col col-12 px2">
            <button type="submit" class="btn btn-linkedin hover-fade"><i class="fab fa-linkedin-in"></i>เข้าสู่ระบบด้วย LinkedIn</button>
          </div>
        </div>

        <div class="sm-col col-12 px2">
          <h4 class="or strike"><span>หรือ</span></h4>
          <h1>เข้าสู่ระบบด้วยอีเมล</h1>
        </div>

        <form id="theForm" class="ml-form login-form" action="thank-you.php" method="POST">
          <div class="clearfix">
            <div class="sm-col col-12 px2">
              <div class="form-item">
                <label class="label" for="email">อีเมล</label>
                <input class="ml-input" name="email" type="email">
              </div>
            </div>
            <div class="sm-col col-12 px2">
              <div class="form-item">
                <label class="label" for="password">รหัสผ่าน</label>
                <input class="ml-input" name="password" type="password">
              </div>
            </div>

            <div class="sm-col col-12 px2">
              <div class="flex">
                <label class="form-item form-item-checkbox remember-me">จดจำฉัน
                  <input name="can_negotiate" type="checkbox" checked="checked">
                  <span class="checkmark"></span>
                </label>
                <a href="forgot-password.php" class="forgot-password ml-link">ลืมรหัสผ่าน?</a>
              </div>

            </div>
            <div class="sm-col col-12 px2">
              <button type="submit" class="btn btn-submit">เข้าสู่ระบบ</button>
            </div>
          </div>
        </form>

        <div class="sm-col col-12 px2 bottom-message">
          ยังไม่มีบัญชีผู้ใช้ใช่ไหม <a href="register.php" class="ml-link underline"><strong>ลงทะเบียน</strong></a>
        </div>

      </div>
    </div>
  </div>
</div>
<script>
  $(function () {
    $('#theForm').validate({
      rules: {
        email: {
          required: true,
        },
        password: {
          required: true,
        }
      },
    });
  });
</script>

<?php require_once("components/footer.php"); ?>
