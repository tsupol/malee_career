<?php
require_once("components/header.php");
require_once ("components/social_share.php");
?>
<div class="ml-page page-apply-job">
  <div class="sec-banner"></div>
  <div class="layout-outer">
    <div class="layout-inner">
      <ul class="breadcrumb">
        <li><a href="index.php">หน้าแรก</a></li>
        <li><a href="work-with-us.php">ร่วมงานกับมาลี</a></li>
        <li><a href="#">Web Developer</a></li>
        <li><a href="apply-job">สมัครงาน</a></li>
        <li><a class="active">ส่งใบสมัครงาน</a></li>
      </ul>
      <h1 class="heading1 centered">สมัครงาน</h1>
      <div class="wws-content-wrap">

        <div class="profile-info">
          <div class="__left">
            <img src="<?php echo $asset_path ?>imgs/avatar-placeholder.jpg">
          </div>
          <div class="__right">
            <div class="ml-px2">
              <h1>Tharadorl Test (Dol)</h1>
              <div class="__row">
                <div class="__col __label">ตำแหน่งงานล่าสุด</div>
                <div class="__col __desc">Project Manager</div>
              </div>
              <div class="__row">
                <div class="__col __label">การศึกษา</div>
                <div class="__col __desc">ปริญญาตรี คณะวิศวกรรมศาสตร์ สาขาซอฟต์แวร์และความรู้ มหาวิทยาลัยเกษตรศาสตร์</div>
              </div>
              <div class="__row">
                <div class="__col __label">เบอร์ติดต่อ</div>
                <div class="__col __desc">095 777 7777</div>
              </div>
              <div class="__row">
                <div class="__col __label">อีเมล</div>
                <div class="__col __desc">mail@mail.com</div>
              </div>
              <div class="__row pad-top">
                <div class="__col __label">เลขที่ใบสมัคร</div>
                <div class="__col __desc">APP1220180526</div>
              </div>
              <div class="__row">
                <div class="__col __label">วันเวลาที่สมัคร</div>
                <div class="__col __desc">2018/05/26  10:15:25am</div>
              </div>
              <div class="__row">
                <div class="__col __label">ตำแหน่งที่สมัคร</div>
                <div class="__col __desc">Project Manager</div>
              </div>
              <div class="buttons">
                <a class="btn-underline" href="#"><i class="fas fa-download"></i>ดาวน์โหลดใบสมัคร</a>
                <a class="btn-underline" href="#"><i class="fas fa-print"></i>พิมพ์ใบสมัคร</a>
              </div>
              <div class="buttons-2">
                <a class="btn" href="#">ย้อนกลับ</a>
                <a class="btn primary" href="#">ส่งใบสมัคร</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $(function () {
    $('#theForm').validate();
  });
</script>

<?php require_once("components/footer.php"); ?>
