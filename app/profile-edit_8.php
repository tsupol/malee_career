<?php require_once("components/header.php"); ?>
<div class="ml-page page-profile">
  <div class="sec-banner-2"></div>
  <div class="layout-outer">
    <div class="layout-inner">
      <div class="layout-profile-page">

        <?php
        require_once('components/profile_nav.php');
        get_profile_nav(2)
        ?>

        <div class="sec-content">
          <h1 class="heading1">แก้ไขโปรไฟล์</h1>
          <?php
          $malee_has_profile_image = true;
          $PROFILE_PAGE = true;
          require_once('register_steps/step_8.php');
          ?>
        </div>
      </div>
      <div class="flex-center">

      </div>
    </div>
  </div>
</div>

<script>
  $(function () {
    $('#theForm').validate();
  });
</script>

<?php require_once("components/footer.php"); ?>
