<?php require_once("components/header.php"); ?>
<div class="ml-page page-forgot">
  <div class="layout-outer">
    <div class="layout-inner">
      <h1 class="heading1 centered">ลืมรหัสผ่าน</h1>
      <h3 class="heading3 centered">กรอกอีเมลที่ใช้สมัครสมาชิก เราจะส่งอีเมลบัญชีผู้ใช้<br class="br-md-m"/> และลิงค์สำหรับเปลี่ยนรหัสผ่านให้คุณ</h3>
    </div>
  </div>
  <div class="layout-outer">
    <div class="layout-inner flex-center">
      <div class="layout-narrow pad-top-2">

        <form id="theForm" class="ml-form login-form" action="./" method="POST">
          <div class="clearfix">
            <div class="sm-col col-12 px2">
              <div class="form-item">
                <label class="label" for="email">อีเมล</label>
                <input class="ml-input" name="email" type="email">
              </div>
            </div>
            <div class="sm-col col-12 px2">
              <button type="submit" class="btn no-margin">ยืนยัน</button>
            </div>
          </div>
        </form>

        <div class="sm-col col-12 px2 bottom-message">
          ถ้าคุณต้องการความช่วยเหลือ กรุณา  <a href="contact.php" class="ml-link underline bold">ติดต่อ HR</a>
        </div>

      </div>
    </div>
  </div>
</div>
<script>
  $(function () {
    $('#theForm').validate({
      rules: {
        email: {
          required: true,
        },
        password: {
          required: true,
        }
      },
    });
  });
</script>

<?php require_once("components/footer.php"); ?>
