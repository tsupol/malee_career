<div class="layout-outer sec-subscribe">
  <div class="layout-inner">
    <div class="__content">
      <h1>STAY UPDATED</h1>
      <h3>Be the first to hear about <span class="color2">Malee</span></h3>
      <div class="btn-subscribe pop-subscribe">SUBSCRIBE</div>
    </div>
  </div>
</div>
<div class="footer">
  <div class="footer-inner">
    <div class="__row nav-wrap">
      <a href="index.php" class="nav-item">หน้าแรก</a>
      <a href="work-with-us.php" class="nav-item">ร่วมงานกับมาลี</a>
      <a href="activity.php" class="nav-item">กิจกรรมในครอบครัว</a>
      <a href="contact.php" class="nav-item">ติดต่อ HR</a>
      <a href="login.php" class="nav-item">เข้าสู่ระบบ</a>
      <a href="register.php" class="nav-item">ลงทะเบียน</a>
    </div>
    <div class="__row social-wrap">
      <a href="#" class="social-item"><i class="fab fa-facebook-f"></i></a>
      <a href="#" class="social-item"><i class="fab fa-twitter"></i></a>
      <a href="#" class="social-item"><i class="fab fa-instagram"></i></a>
      <a href="#" class="social-item"><i class="fab fa-youtube"></i></a>
      <a href="#" class="social-item"><i class="fab fa-linkedin"></i></a>
      <a href="#" class="social-item"><div class="icon-line"></div></a>
    </div>
    <div class="__row copyright-wrap font2">
      COPYRIGHT (C) 2018 Malee Group<br class="xsm-br">CO.,LTD. ALL RIGHT RESERVED.
    </div>
    <div class="__row footer-bottom">
      <a href="terms-conditions.php" class="nav-item">Terms & Conditions</a>
      <a href="#" class="nav-item">Privacy Policy</a>
    </div>

    <!-- Back to top-->
    <div class="back-to-top"></div>

  </div>
</div>
<!------------ Scripts ------------>
<!-- External -->
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script src="<?php echo $asset_path ?>/js/datepicker/dist/datepicker.min.js"></script>
<!-- Main -->
<script src="<?php echo $asset_path ?>/js/index.js?<?php echo time(); ?>"></script>
<script src="<?php echo $asset_path ?>/js/popup.js?<?php echo time(); ?>"></script>
<?php echo "</body></html>" ?>

