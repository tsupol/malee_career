<?php function get_steps($active_step = 1, $mobile_title = "untitled") { ?>
  <div class="reg-steps">
    <div class="full-steps">
      <div class="__step<?php echo $active_step == 1 ? ' active' : '' ?>">
        <div class="__number">1</div>
        <div class="__label">ประวัติส่วนตัว</div>
      </div>
      <div class="__step<?php echo $active_step == 2 ? ' active' : '' ?>">
        <div class="__number">2</div>
        <div class="__label">การศึกษา</div>
      </div>
      <div class="__step<?php echo $active_step == 3 ? ' active' : '' ?>">
        <div class="__number">3</div>
        <div class="__label">ประสบการณ์ทำงาน</div>
      </div>
      <div class="__step<?php echo $active_step == 4 ? ' active' : '' ?>">
        <div class="__number">4</div>
        <div class="__label">อบรมหรือฝึกงาน</div>
      </div>
      <div class="__step<?php echo $active_step == 5 ? ' active' : '' ?>">
        <div class="__number">5</div>
        <div class="__label">ความสามารถพิเศษ</div>
      </div>
      <div class="__step<?php echo $active_step == 6 ? ' active' : '' ?>">
        <div class="__number">6</div>
        <div class="__label">อาชญากรรมและสุขภาพ</div>
      </div>
      <div class="__step<?php echo $active_step == 7 ? ' active' : '' ?>">
        <div class="__number">7</div>
        <div class="__label">บุคคลอ้างอิง</div>
      </div>
      <div class="__step<?php echo $active_step == 8 ? ' active' : '' ?>">
        <div class="__number">8</div>
        <div class="__label">เสร็จสิ้น</div>
      </div>
    </div>
    <div class="mini-step">
      <div class="__number">
        <span class="big"><?php echo $active_step ?></span>of
        <span class="total">8</span>
      </div>
      <div class="__label"><?php echo $mobile_title ?></div>
    </div>
  </div>
<?php } ?>
