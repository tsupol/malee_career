<?php function get_job_filter_table($with_pagination = false) { ?>

  <div class="job-filter-table">

    <!-- Filter -->

    <div class="job-filter clearfix ml-form">

      <div class="jf-col jf-col1">
        <div class="form-item small">
          <select class="ml-input" name="unnamed">
            <option disabled selected value>ประเภทงาน</option>
            <?php include('components/select_options.php') ?>
          </select>
        </div>
      </div>
      <div class="jf-col jf-col2">
        <div class="form-item small">
          <select class="ml-input" name="unnamed">
            <option disabled selected value>สถานที่ทำงาน</option>
            <?php include('components/select_options.php') ?>
          </select>
        </div>
      </div>
      <div class="jf-col jf-col3">
        <div class="form-item small">
          <select class="ml-input" name="unnamed">
            <option disabled selected value>วันที่</option>
            <?php include('components/select_options.php') ?>
          </select>
        </div>
      </div>
      <div class="jf-col jf-col4">
        <div class="form-item small">
          <input class="ml-input" name="unnamed" type="text" placeholder="ค้นหางาน...">
        </div>
      </div>
      <div class="jf-col jf-col5">
        <div class="btn-outline-d small secondary">ค้นหา</div>
      </div>

    </div>


    <!-- Table -->
    <div class="job-table">
      <h1>ตำแหน่งงานที่เปิดรับสมัคร</h1>
      <div class="table-wrap">

        <?php for ($i = 0; $i < 10; $i++) { ?>
          <div class="__row">
            <a href="job-description.php" class="__col col1">
              <p class="__title">
                <span class="bold">
                  Software Developer / Programmer
                  <?php echo $i%2 == 0 ? '(More Desctiption)' : '' ?>
                </span>
                (Urgent)
              </p>
              <p class="__date">24/05/2561</p>
            </a>
            <div class="__col col2">
              <p class="__count bold" class="bold">1 อัตรา</p>
            </div>
            <div class="__col col3">
              <p class="__company">บริษัท มาลีเอ็นเตอร์ไพรส์ จํากัด</p>
              <p class="__company-branch">สำนักงานรังสิต</p>
            </div>
            <div class="__col col4">
              <p><a href="apply-job.php">สมัครงาน</a></p>
            </div>
            <div class="__col col5">
              <p><a class="pop-share">แชร์</a></p>
            </div>
          </div>
        <?php } ?>

      </div>
    </div>

    <?php if ($with_pagination) { ?>
      <?php include('components/pagination.php') ?>
    <?php } else { ?>

      <div class="see-all-jobs">
        <a href="work-with-us.php" class="btn-outline-d btn-see-all-jobs">ดูตำแหน่งงานทั้งหมด</a>
      </div>

    <?php } ?>

  </div>
<?php } ?>
