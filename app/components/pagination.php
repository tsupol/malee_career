<div class="pagination">
  <a href="#" class="pg-nav pg-prev"></a>
  <select class="pg-select">
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
  </select>
  <div class="pg-total">of 4</div>
  <a href="#" class="pg-nav pg-next"></a>
</div>
