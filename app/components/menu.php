<div class="main-menu minimize">
  <div class="main-menu-inner">
    <a href="index.php" class="__logo flex-center">
      <img src="<?php echo $asset_path ?>imgs/logo-malee.png"/>
    </a>
    <div class="__content">
      <!-- Top -->
      <div class="top-wrap">
        <div class="menu-wrap font2">
          <a href="#" class="menu-item">YOU ARE BUSINESS</a>
          <div class="menu-item-divider"></div>
          <a href="#" class="menu-item">INVESTOR</a>
          <div class="menu-item-divider"></div>
          <a href="#" class="menu-item">CONSUMER</a>
        </div>

        <!-- Auth -->
        <?php if (isset($LOGGED_IN)) { ?>
          <div class="logged-in">
            <div class="_stay">
              <img class="avatar small" src="<?php echo $asset_path ?>imgs/avatar-placeholder.jpg"/>
              <div class="__name">โปรไฟล์</div>
              <div class="__chevron">
                <div class="chevron small bottom"></div>
              </div>
            </div>
            <div class="_drop">
              <div class="arrow_box">
                <a class="__nav" href="profile.php">บัญชีผู้ใช้</a>
                <a class="__nav" href="profile.php">ออกจากระบบ</a>
              </div>
            </div>
          </div>
        <?php } else { ?>
          <div class="auth-menu-wrap">
            <a href="login.php" data-active="login" class="auth-menu-item">เข้าสู่ระบบ</a>
            <div class="menu-item-divider"></div>
            <a href="register.php" data-active="register" class="auth-menu-item">ลงทะเบียน</a>
          </div>
        <?php } ?>

        <!-- Profile -->
        <div class="profile-menu-wrap">
          <a href="#" class="profile-menu-item">
            บัญชีผู้ใช้
          </a>
          <a href="#" class="profile-menu-item">
            ออกจากระบบ
          </a>
        </div>
      </div>

      <!-- Bottom -->
      <div class="bottom-wrap">
        <a href="index.php" data-active="index" class="menu-item">หน้าแรก</a>
        <a href="work-with-us.php" data-active="work-with-us" class="menu-item">ร่วมงานกับมาลี</a>
        <!--        <a href="activity.php" class="menu-item">-->
        <div class="menu-item nav-act-hover" data-active="activity">
          <div class="nav-hover-text" id="nav-activity-item">
            กิจกรรมในครอบครัว

            <!--Mega Menu-->
            <div class="nav-activity hide-click-outside" id="nav-activity">
              <div class="layout-inner __wrap">
                <a class="__title" href="activity.php">กิจกรรมในครอบครัว</a>
                <div class="__nav-wrap">
                  <a href="activity-family.php" data-active="activity-family" class="__nav plain-anchor">กิจกรรมครอบครัวมาลี</a>
                  <a href="activity-develop.php" data-active="activity-develop" class="__nav plain-anchor">กิจกรรมพัฒนาต่างๆ</a>
                  <a href="activity-csr.php" data-active="activity-csr" class="__nav plain-anchor">กิจกรรม CSR</a>
                  <a href="activity-joint.php" data-active="activity-joint" class="__nav plain-anchor">กิจกรรมร่วมกับสถาบันภายนอก</a>
                </div>
              </div>
            </div>

          </div>
        </div>
        <a href="contact.php" data-active="contact" class="menu-item">ติดต่อ HR</a>
        <div class="menu-lang">ไทย
          <div class="__chevron">
            <div class="chevron small bottom"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Mobile -->
<div class="mobile-menu">
  <!-- Profile Menu -->
  <!-- Note: add class `show` to display by default -->
  <!--  <div class="mm-profile hide-click-outside">-->
  <div class="mm-profile  <?php echo $LOGGED_IN ? 'show' : '' ?>">
    <div class="mm-label _nav">ภาพรวมบัญชีผู้ใช้</div>
    <a class="_nav" href="profile-edit.php">แก้ไขโปรไฟล์</a>
    <a class="_nav" href="profile-history.php">ประวัติการสมัคร</a>
    <a class="_nav" href="profile-settings.php">การตั้งค่า</a>
    <a class="_nav" href="profile-settings.php">ออกจากระบบ</a>
    <div class="_chevron">
      <div class="chevron small bottom"></div>
    </div>
  </div>

  <!-- Stay -->
  <div class="mm-floated-wrap">
    <div class="hamburger">
      <div class="bar1"></div>
      <div class="bar2"></div>
      <div class="bar3"></div>
    </div>
    <a href="index.php" class="mm-logo">
      <img src="<?php echo $asset_path ?>imgs/logo-malee.png"/>
    </a>

    <!-- Auth -->
    <?php if (isset($LOGGED_IN)) { ?>
      <div class="logged-in-m">
        <div class="_stay">
          <img class="avatar small" src="<?php echo $asset_path ?>imgs/avatar-placeholder.jpg"/>
        </div>
        <!--        <div class="_drop">-->
        <!--          <div class="arrow_box">-->
        <!--            <a class="__nav" href="profile.php">บัญชีผู้ใช้</a>-->
        <!--            <a class="__nav" href="profile.php">ออกจากระบบ</a>-->
        <!--          </div>-->
        <!--        </div>-->
      </div>
    <?php } ?>

  </div>

  <!-- Drop -->
  <div class="mm-content-wrap">
    <ul class="mm-accordion">
      <li data-active="index"><a href="index.php" class="mm-menu-item">หน้าแรก</a></li>
      <li data-active="work-with-us"><a href="work-with-us.php" class="mm-menu-item">ร่วมงานกับมาลี</a></li>
      <li class="expandable">
        <div class="__label-wrap">
          <div class="mm-menu-item">
            กิจกรรมในครอบครัว
            <div class="chevron"></div>
          </div>
        </div>
        <div class="nested-wrap">
          <ul class="mm-accordion-nested">
            <li data-active="activity-family"><a href="activity-family.php" class="mm-menu-item">กิจกรรมครอบครัวมาลี</a></li>
            <li data-active="activity-develop"><a href="activity-develop.php" class="mm-menu-item">กิจกรรมพัฒนาต่างๆ</a></li>
            <li data-active="activity-csr"><a href="activity-csr.php" class="mm-menu-item">กิจกรรม CSR</a></li>
            <li data-active="activity-joint"><a href="activity-joint.php" class="mm-menu-item">กิจกรรมโครงการร่วมกับสถาบัน<br/>การศึกษาภายนอก</a></li>
          </ul>
        </div>
      </li>
      <li data-active="contact"><a href="contact.php" class="mm-menu-item">ติดต่อ HR</a></li>
      <li data-active="login"><a href="login.php" class="mm-menu-item">เข้าสู่ระบบ</a></li>
      <li data-active="register"><a href="register.php" class="mm-menu-item">ลงทะเบียน</a></li>
      <li>
        <a href="#" class="mm-menu-item font3"
           style="font-weight: normal">Sitemap</a>
      </li>
    </ul>

    <!-- You Are-->
    <div class="yr-wrap">
      <a href="#" class="yr-menu-item"><span class="yr-underline">YOU ARE BUSINESS</span></a>
      <a href="#" class="yr-menu-item"><span class="yr-underline">YOU ARE INVESTOR</span></a>
      <a href="#" class="yr-menu-item"><span class="yr-underline">YOU ARE CONSUMER</span></a>
    </div>

    <!-- Subscribe -->
    <div class="stay-updated">
      <h1>STAY UPDATED</h1>
      <h3>Be the first to hear about <span class="color-danger">Malee</span></h3>
      <a href="#" class="mm-btn-subscribe pop-subscribe">SUBSCRIBE</a>
    </div>

    <!-- Bottom -->
    <div class="mm-bottom">
      <div class="mm-lang">
        <a href="#" class="lang-menu-item active">ไทย</a>
        <a href="#" class="lang-menu-item">Eng</a>
        <a href="#" class="lang-menu-item">中文</a>
      </div>
      <a href="#" class="mm-chat">
        <div class="chat-img"></div>
        <span>CHAT WITH US</span>
      </a>
    </div>
  </div>
</div>

