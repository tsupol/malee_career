<div id="popup-subscribe" class="ml-popup">
  <div class="back-drop close-popup"></div>
  <div class="mp-inner">
    <div class="sp-close close-popup">×</div>

    <!-- Begin Content -->
    <!-- Note: the class `step2` is for switching to newsletters selection -->
    <div class="mp-content centered">

      <!-- Step 1 email -->
      <div class="subc-step1">
        <h1 class="heading1">ติดตามข่าวสาร</h1>
        <h3 class="heading3">
          รับข่าวสารเป็นคนแรกจาก
          <span class="color-font">มาลี</span>
        </h3>
        <div class="mp-form form-subsc">
          <input class="mp-input input-subsc" type="text" placeholder="ENTER YOUR EMAIL ADDRESS" />
          <div class="btn btn-outline" id="btn-subscribe">SUBSCRIBE</div>
        </div>
        <p class="or">
          หรือ
        </p>
        <div class="socials-subsc">
          <a href="#" class="social-item color-facebook"><i class="fab fa-facebook"></i></a>
          <a href="#" class="social-item color-linkedin"><i class="fab fa-linkedin"></i></a>
        </div>
      </div>

      <!-- Step 2 Choose Newsletters -->
      <div class="subc-step2">
        <h1 class="heading1">ขอบคุณ</h1>
        <h3 class="heading3">เลือกประเภทข่าวสารที่ต้องการรับ</h3>
        <div class="mp-form ml-form form-subsc2">
          <div class="ml-col col-12 sm-col-6">
            <label class="form-item form-item-checkbox">ตำแหน่งงานที่เปิดรับสมัคร
              <input name="newsletter" type="checkbox" checked="checked">
              <span class="checkmark"></span>
            </label>
          </div>
          <div class="ml-col col-12 sm-col-6">
            <label class="form-item form-item-checkbox">กิจกรรมโครงการร่วมกับ สถาบันการศึกษาภายนอก
              <input name="newsletter" type="checkbox" checked="checked">
              <span class="checkmark"></span>
            </label>
          </div>
          <div class="ml-col col-12 sm-col-6">
            <label class="form-item form-item-checkbox">กิจกรรม CSR
              <input name="newsletter" type="checkbox">
              <span class="checkmark"></span>
            </label>
          </div>
          <div class="ml-col col-12 sm-col-6">
            <label class="form-item form-item-checkbox">กิจกรรมครอบครัวมาลี
              <input name="newsletter" type="checkbox" checked="checked">
              <span class="checkmark"></span>
            </label>
          </div>
          <div class="ml-col col-12 sm-col-6">
            <label class="form-item form-item-checkbox">กิจกรรมพัฒนาต่างๆ
              <input name="newsletter" type="checkbox" checked="checked">
              <span class="checkmark"></span>
            </label>
          </div>
          <div class="ml-col col-12 sm-col-6">
            <label class="form-item form-item-checkbox">อื่นๆ
              <input name="newsletter" type="checkbox">
              <span class="checkmark"></span>
            </label>
          </div>
          <div class="btn-wrap">
            <div class="btn btn-outline">SUBMIT</div>
          </div>
        </div>
      </div>

    </div>
    <!-- End Content -->

  </div>
</div>

<!-- For switching to newsletter selection -->
<script>
  $(function () {
    $('#btn-subscribe').click(function () {
      $('#popup-subscribe .mp-content').addClass('step2')
    });
  });
</script>
