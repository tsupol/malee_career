<div id="popup-share" class="ml-popup">
  <div class="back-drop close-popup"></div>
  <div class="mp-inner">
    <div class="sp-close close-popup">×</div>

    <!-- Begin Content -->
    <div class="mp-content centered">
      <h1 class="heading1">แชร์</h1>
      <h2 class="heading2">ตำแหน่ง Web Developer (Urgent)</h2>
      <div class="socials">
        <a href="#" class="social-item bg-facebook"><i class="fab fa-facebook-f"></i></a>
        <a href="#" class="social-item bg-twitter"><i class="fab fa-twitter"></i></a>
        <a href="#" class="social-item bg-linkedin"><i class="fab fa-linkedin-in"></i></a>
        <a href="#" class="social-item bg-email"><i class="fas fa-envelope"></i></a>
      </div>
      <div class="mp-form form-share">
        <input class="mp-input input-share" type="text" value="https://www.malee.co.th/en" />
        <div class="btn-copy">COPY</div>
      </div>
    </div>
    <!-- End Content -->

  </div>
</div>
