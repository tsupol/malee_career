<div id="popup-thank-register" class="ml-popup">
  <div class="back-drop close-popup"></div>
  <div class="mp-inner small">
    <div class="sp-close close-popup">×</div>

    <!-- Begin Content -->
    <!-- Note: the class `step2` is for switching to newsletters selection -->
    <div class="mp-content centered">

      <h1 class="heading1">ขอบคุณ</h1>
      <br/>
      <h3 class="heading3">
        เราได้รับข้อมูลของคุณเรียบร้อยแล้ว
      </h3>
    </div>
    <!-- End Content -->

  </div>
</div>

<!-- For switching to newsletter selection -->
<script>
  $(function () {
    $('#btn-subscribe').click(function () {
      $('#popup-subscribe .mp-content').addClass('step2')
    });
  });
</script>
