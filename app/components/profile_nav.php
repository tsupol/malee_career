<?php
function get_profile_nav($index = 1) {
  global $asset_path; ?>

  <div class="sec-nav">
    <div class="avatar-wrap flex-v-center">
      <img class="avatar" src="<?php echo $asset_path ?>imgs/avatar-placeholder.jpg"/>
      <h1>Tharadorl Test</h1>
      <h3>mail@mail.com</h3>
    </div>
    <div class="nav-wrap">
      <a href="profile.php" class="nav-item<?php echo $index == 1 ? ' active' : '' ?>">ภาพรวมบัญชีผู้ใช้</a>
      <a href="profile-edit.php" class="nav-item<?php echo $index == 2 ? ' active' : '' ?>">แก้ไขโปรไฟล์</a>
      <a href="profile-history.php" class="nav-item<?php echo $index == 3 ? ' active' : '' ?>">ประวัติการสมัครงาน</a>
      <a href="profile-settings.php" class="nav-item<?php echo $index == 4 ? ' active' : '' ?>">การตั้งค่า</a>
    </div>
  </div>

<?php } ?>
