<div class="social-share">
  <div class="desktop">
    <a href="#" class="ss-item fb"><i class="fab fa-facebook-f"></i></a>
    <a href="#" class="ss-item tw"><i class="fab fa-twitter"></i></i></a>
    <a href="#" class="ss-item li"><i class="fab fa-linkedin-in"></i></a>
    <a href="#" class="ss-item gp"><i class="fab fa-google-plus-g"></i></a>
  </div>
</div>

<div class="social-share-mobile hide-click-outside">
  <div class="ssm-slide">
    <div class="share-txt">แชร์</div>
    <a href="#" class="ssm-item fb"><i class="fab fa-facebook-f"></i></a>
    <a href="#" class="ssm-item tw"><i class="fab fa-twitter"></i></i></a>
    <a href="#" class="ssm-item li"><i class="fab fa-linkedin-in"></i></a>
    <a href="#" class="ssm-item link"><i class="fas fa-link"></i></a>
    <div class="smm-share-icon">
      <i class="fas fa-share-alt"></i>
    </div>
  </div>
</div>
