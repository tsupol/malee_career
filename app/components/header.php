<?php
$asset_path = "./";
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Title</title>
  <meta charset="UTF-8">
  <meta name="description" content="Malee">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <link rel="canonical" href="http://malee.artplore.com" />

  <!--  Fonts -->

  <link rel="stylesheet" type="text/css" media="all" href="<?php echo $asset_path ?>fonts/kalatexatext/stylesheet.css"/>
  <link rel="stylesheet" type="text/css" media="all" href="<?php echo $asset_path ?>fonts/renh/stylesheet.css"/>
  <link href="https://fonts.googleapis.com/css?family=Kanit:400,500" rel="stylesheet"/>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
        integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous" />

  <!-- External -->

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" media="all" href="<?php echo $asset_path ?>js/datepicker/dist/datepicker.css"/>
  <link href="https://unpkg.com/basscss@8.0.2/css/basscss.min.css" rel="stylesheet">

  <!-- Main -->
  <link rel="stylesheet" type="text/css" media="all" href="<?php echo $asset_path ?>css/main.css?<?php echo time(); ?>"/>

</head>
<body>
<div class="nav-pad"></div>
<?php
require_once ("menu.php");
// popups
require_once ("popups/share.php");
require_once ("popups/subscribe.php");
require_once ("popups/register_thankyou.php");
