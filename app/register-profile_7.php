<?php require_once("components/header.php"); ?>
<div class="ml-page page-reg-profile">
  <div class="layout-outer">
    <div class="layout-inner">
      <h1 class="heading1 centered">สร้างโปรไฟล์</h1>
      <div class="flex-center">
        <?php
        require_once('register_steps/step_7.php');
        ?>
      </div>
    </div>
  </div>
</div>

<script>
  $(function () {
    $('#theForm').validate();
  });
</script>

<?php require_once("components/footer.php"); ?>
