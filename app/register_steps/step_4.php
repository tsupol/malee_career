<div class="steps-and-form">
  <?php
  require_once('components/steps.php');
  get_steps(4, 'อบรมหรือฝึกงาน')
  ?>
  <form id="theForm" class="ml-form form-profile" action="./" method="POST">
    <h2 class="__step-title">ประวัติการอบรมหรือฝึกงาน</h2>
    <div class="form-profile-inner">
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">หัวข้อ/หลักสูตร</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">สถาบัน</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <select class="ml-input" name="unnamed" id="unnamed">
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">ปี พ.ศ.</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="btn">+ เพิ่มประวัติการอบรมหรือฝึกงาน</div>
      </div>
    </div>

    <div class="profile-reg-btns">

      <a type="submit" class="btn btn-profile-reg prev"
         href="<?php echo isset($PROFILE_PAGE) ? 'profile-edit_3.php' : 'register-profile_3.php' ?>">
        กลับ
      </a>
      <button type="submit" class="btn btn-profile-reg">บันทึก</button>
      <a type="submit" class="btn btn-profile-reg next"
         href="<?php echo isset($PROFILE_PAGE) ? 'profile-edit_5.php' : 'register-profile_5.php' ?>">
        ต่อไป
      </a>
    </div>

  </form>
</div>
<script>
  $(function () {
    $('#theForm').validate();
  });
</script>
