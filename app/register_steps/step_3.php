<div class="steps-and-form">
  <?php
  require_once('components/steps.php');
  get_steps(3, 'ประสบการณ์ทำงาน')
  ?>
  <form id="theForm" class="ml-form form-profile" action="./" method="POST">
    <h2 class="__step-title">ประวัติการทำงานล่าสุด</h2>
    <div class="form-profile-inner">
      <div class="ml-col col-6">
        <div class="form-item">
          <select class="ml-input" name="unnamed" id="unnamed">
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">ระยะเวลาตั้งแต่เดือน</label>
        </div>
      </div>
      <div class="ml-col col-6">
        <div class="form-item">
          <select class="ml-input" name="unnamed" id="unnamed">
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">ปี พ.ศ.</label>
        </div>
      </div>
      <div class="ml-col col-6">
        <div class="form-item">
          <select class="ml-input" name="unnamed" id="unnamed">
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">ถึงเดือน</label>
        </div>
      </div>
      <div class="ml-col col-6">
        <div class="form-item">
          <select class="ml-input" name="unnamed" id="unnamed">
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">ปี พ.ศ.</label>
        </div>
      </div>

      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">บริษัท</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">ตำแหน่งเริ่มต้น</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">ตำแหน่งสุดท้าย</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">เงินเดือนสุดท้าย</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">เหตุผลในการลาออก/เปลี่ยนงาน</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="btn">+ เพิ่มประวัติการทำงาน</div>
      </div>
    </div>

    <div class="profile-reg-btns">

      <a type="submit" class="btn btn-profile-reg prev"
         href="<?php echo isset($PROFILE_PAGE) ? 'profile-edit_2.php' : 'register-profile_2.php' ?>">
        กลับ
      </a>
      <button type="submit" class="btn btn-profile-reg">บันทึก</button>
      <a type="submit" class="btn btn-profile-reg next"
         href="<?php echo isset($PROFILE_PAGE) ? 'profile-edit_4.php' : 'register-profile_4.php' ?>">
        ต่อไป
      </a>
    </div>

  </form>
</div>
<script>
  $(function () {
    $('#theForm').validate();
  });
</script>
