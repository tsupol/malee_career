<div class="steps-and-form">
  <?php
  require_once('components/steps.php');
  get_steps(1, 'ประวัติส่วนตัว')
  ?>
  <form id="theForm" class="ml-form login-form form-profile" action="./" method="POST">
    <div class="form-profile-inner">

      <div class="sm-col col-12 px2">
        <?php if (isset($malee_has_profile_image)) { ?>
          <div class="flex-center">
            <div class="avatar-dropzone-edit">
              <div class="edit-btn">+ แก้ไขรูปถ่าย</div>
            </div>
          </div>
        <?php } else { ?>
          <div class="avatar-dropzone"></div>
        <?php } ?>
      </div>
      <div class="sm-col col-12 px2">
        <div class="form-item">
          <select class="ml-input" name="unnamed" required>
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">คำนำหน้าชื่อ</label>
        </div>
      </div>
      <div class="sm-col col-12 px2">
        <div class="form-item">
          <input class="ml-input" name="first_name" type="text" required>
          <label class="label" for="first_name">ชื่อ (ภาษาไทย)</label>
        </div>
      </div>
      <div class="sm-col col-12 px2">
        <div class="form-item">
          <input class="ml-input" name="last_name" type="text" required>
          <label class="label" for="last_name">นามสกุล (ภาษาไทย)</label>
        </div>
      </div>
      <div class="sm-col col-12 px2">
        <div class="form-item">
          <input class="ml-input" name="unnamed" type="text" required>
          <label class="label" for="unnamed">ชื่อ (ภาษาอังกฤษ)</label>
        </div>
      </div>
      <div class="sm-col col-12 px2">
        <div class="form-item">
          <input class="ml-input" name="unnamed" type="text" required>
          <label class="label" for="unnamed">นามสกุล (ภาษาอังกฤษ)</label>
        </div>
      </div>
      <div class="sm-col col-12 px2">
        <div class="form-item">
          <input class="ml-input" name="unnamed" type="text">
          <label class="label" for="unnamed">ชื่อเล่น</label>
        </div>
      </div>
      <div class="sm-col col-12 px2">
        <div class="form-item">
          <input class="ml-input" name="unnamed" type="text">
          <label class="label" for="unnamed">หมายเลขบัตรประจำตัวประชาชน/เลขที่หนังสือเดินทาง</label>
        </div>
      </div>

      <!-- Date -->
      <div class="sm-col col-4 px2">
        <div class="form-item">
          <select class="ml-input" name="unnamed">
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">วันที่</label>
        </div>
      </div>
      <div class="sm-col col-4 px2">
        <div class="form-item">
          <select class="ml-input" name="unnamed">
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">เดือน</label>
        </div>
      </div>
      <div class="sm-col col-4 px2">
        <div class="form-item">
          <select class="ml-input" name="unnamed">
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">ปีเกิด</label>
        </div>
      </div>
      <!-- End Date -->

      <div class="sm-col col-12 px2">
        <div class="form-item">
          <select class="ml-input" name="unnamed" required>
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">เพศ</label>
        </div>
      </div>
      <div class="sm-col col-12 px2">
        <div class="form-item">
          <select class="ml-input" name="unnamed" disabled>
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">สถานะทางการทหาร (เพศชาย)</label>
        </div>
      </div>
      <div class="sm-col col-12 px2">
        <div class="form-item">
          <input class="ml-input" name="unnamed" type="text" required>
          <label class="label" for="unnamed">ที่อยู่ปัจจุบัน</label>
        </div>
      </div>
      <div class="sm-col col-12 px2">
        <div class="form-item">
          <select class="ml-input" name="unnamed">
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">จังหวัด</label>
        </div>
      </div>
      <div class="sm-col col-12 px2">
        <div class="form-item">
          <select class="ml-input" name="unnamed">
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">อำเภอ/เขต</label>
        </div>
      </div>
      <div class="sm-col col-12 px2">
        <div class="form-item">
          <select class="ml-input" name="unnamed">
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">ตำบล/แขวง</label>
        </div>
      </div>
      <div class="sm-col col-12 px2">
        <div class="form-item">
          <label class="label" for="unnamed">รหัสไปรษณีย์</label>
          <input class="ml-input" name="unnamed" type="text">
        </div>
      </div>

      <!-- Phone -->
      <div class="sm-col col-6 px2">
        <div class="form-item">
          <label class="label" for="unnamed">เบอร์โทรศัพท์</label>
          <input class="ml-input" name="unnamed" type="text">
        </div>
      </div>
      <div class="sm-col col-6 px2">
        <div class="form-item">
          <label class="label" for="unnamed">เบอร์โทรศัพท์มือถือ</label>
          <input class="ml-input" name="unnamed" type="text" required>
        </div>
      </div>

      <div class="sm-col col-12 px2">
        <div class="form-item">
          <select class="ml-input" name="unnamed" required>
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">สถานะการสมรส</label>
        </div>
      </div>

      <div class="sm-col col-12 px2">
        <div class="form-item">
          <input class="ml-input" name="unnamed" type="text" disabled>
          <label class="label" for="unnamed">ชื่อคู่สมรส</label>
        </div>
      </div>
      <div class="sm-col col-12 px2">
        <div class="form-item">
          <input class="ml-input" name="unnamed" type="text" disabled>
          <label class="label" for="unnamed">นามสกุลคู่สมรส</label>
        </div>
      </div>
      <div class="sm-col col-12 px2">
        <div class="form-item">
          <input class="ml-input" name="unnamed" type="text" disabled>
          <label class="label" for="unnamed">อาชีพคู่สมรส</label>
        </div>
      </div>

      <div class="sm-col col-12 px2">
        <div class="form-item">
          <select class="ml-input" name="unnamed" disabled>
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">จำนวนบุตร</label>
        </div>
      </div>

      <div class="sm-col col-12 px2">
        <div class="form-item">
          <input class="ml-input" id="file-upload" name="file-upload" type="file">
          <label for="file-upload" class="file-label">
            <i class="fas fa-cloud-upload-alt"></i>แนบเอกสาร<i class="fas fa-upload"></i>
          </label>
        </div>
        <p class="force-desc">ขนาดของไฟล์ไม่เกิน 200 Kb. ในรูปแบบ Word(.doc/.docx), PDF(.pdf), Picture(.jpg/
          .gif/.png)</p>
      </div>

    </div>

    <div class="profile-reg-btns">
      <button type="submit" class="btn btn-profile-reg">บันทึก</button>
      <a type="submit" class="btn btn-profile-reg next"
         href="<?php echo isset($PROFILE_PAGE) ? 'profile-edit_2.php' : 'register-profile_2.php' ?>">
        ต่อไป
      </a>
    </div>

  </form>
</div>

<script>
  $(function () {
    $('#theForm').validate();
  });
</script>
