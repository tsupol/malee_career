<div class="steps-and-form">
  <?php
  require_once('components/steps.php');
  get_steps(6, 'อาชญากรรมและสุขภาพ')
  ?>
  <form id="theForm" class="ml-form form-profile" action="./" method="POST">
    <div class="form-profile-inner">
      <!-- Question -->
      <div class="__question-wrap">
        <h4 class="__question">1. ท่านมีร่างกายทุพพลภาพ หรือเป็นโรคติดต่อหรือไม่?</h4>
        <div class="ml-col col-6">
          <label class="form-item form-item-radio">มี
            <input type="radio" name="q1">
            <span class="checkmark"></span>
          </label>
        </div>
        <div class="ml-col col-6">
          <label class="form-item form-item-radio">ไม่มี
            <input type="radio" name="q1">
            <span class="checkmark"></span>
          </label>
        </div>
      </div>

      <!-- Question -->
      <div class="__question-wrap">
        <h4 class="__question">2. ท่านเคยถูกปลดออกจากงานหรือไม่?</h4>
        <div class="ml-col col-6">
          <label class="form-item form-item-radio">เคย
            <input type="radio" name="q2">
            <span class="checkmark"></span>
          </label>
        </div>
        <div class="ml-col col-6">
          <label class="form-item form-item-radio">ไม่เคย
            <input type="radio" name="q2">
            <span class="checkmark"></span>
          </label>
        </div>
      </div>

      <!-- Question -->
      <div class="__question-wrap">
        <h4 class="__question">3. ท่านเคยถูกกล่าวหาหรือต้องโทษในคดีอาญาหรือไม่?</h4>
        <div class="ml-col col-6">
          <label class="form-item form-item-radio">เคย
            <input type="radio" name="q3">
            <span class="checkmark"></span>
          </label>
        </div>
        <div class="ml-col col-6">
          <label class="form-item form-item-radio">ไม่เคย
            <input type="radio" name="q3">
            <span class="checkmark"></span>
          </label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text" disabled>
          <label class="label" for="unnamed">หากตอบคำถามข้างบนว่า มีหรือเคยโปรดแจ้งรายละเอียด</label>
        </div>
      </div>

      <!-- Question -->
      <div class="__question-wrap">
        <h4 class="__question">4. ท่านมีใบอนุญาตขับขี่หรือไม่?</h4>
        <div class="ml-col col-6">
          <label class="form-item form-item-radio">มี
            <input type="radio" name="q4">
            <span class="checkmark"></span>
          </label>
        </div>
        <div class="ml-col col-6">
          <label class="form-item form-item-radio">ไม่มี
            <input type="radio" name="q4">
            <span class="checkmark"></span>
          </label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text" disabled required>
          <label class="label" for="unnamed">หากมีโปรดระบุเลขที่</label>
        </div>
      </div>

      <!-- Question -->
      <div class="__question-wrap">
        <h4 class="__question">5. ท่านมีใบอนุญาตอื่นๆ หรือไม่?</h4>
        <div class="ml-col col-6">
          <label class="form-item form-item-radio">มี
            <input type="radio" name="q5">
            <span class="checkmark"></span>
          </label>
        </div>
        <div class="ml-col col-6">
          <label class="form-item form-item-radio">ไม่มี
            <input type="radio" name="q5">
            <span class="checkmark"></span>
          </label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text" disabled>
          <label class="label" for="unnamed">หากมีโปรดระบุชื่อใบอนุญาต</label>
        </div>
      </div>


      <!-- Question -->
      <div class="__question-wrap">
        <h4 class="__question">6. ท่านเล่นกีฬาหรือมีงานอดิเรกหรือไม่?</h4>
        <div class="ml-col col-6">
          <label class="form-item form-item-radio">มี
            <input type="radio" name="q6">
            <span class="checkmark"></span>
          </label>
        </div>
        <div class="ml-col col-6">
          <label class="form-item form-item-radio">ไม่มี
            <input type="radio" name="q6">
            <span class="checkmark"></span>
          </label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text" disabled>
          <label class="label" for="unnamed">หากมีโปรดระบุ</label>
        </div>
      </div>


      <!-- Question -->
      <div class="__question-wrap">
        <h4 class="__question">7. ท่านดื่มสุราหรือไม่?</h4>
        <div class="ml-col col-6">
          <label class="form-item form-item-radio">ดื่ม
            <input type="radio" name="q7">
            <span class="checkmark"></span>
          </label>
        </div>
        <div class="ml-col col-6">
          <label class="form-item form-item-radio">ไม่ดื่ม
            <input type="radio" name="q7">
            <span class="checkmark"></span>
          </label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text" disabled>
          <label class="label" for="unnamed">ถ้าดื่มโปรดระบุกี่แก้วต่อวัน</label>
        </div>
      </div>


      <!-- Question -->
      <div class="__question-wrap">
        <h4 class="__question">8. ท่านสูบบุหรี่หรือไม่?</h4>
        <div class="ml-col col-6">
          <label class="form-item form-item-radio">สูบ
            <input type="radio" name="q8">
            <span class="checkmark"></span>
          </label>
        </div>
        <div class="ml-col col-6">
          <label class="form-item form-item-radio">ไม่สูบ
            <input type="radio" name="q8">
            <span class="checkmark"></span>
          </label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text" disabled>
          <label class="label" for="unnamed">ถ้าสูบโปรดระบุกี่มวนต่อวัน</label>
        </div>
      </div>


      <!-- Question -->
      <div class="__question-wrap">
        <h4 class="__question">9. ท่านมีโรคประจำตัวหรือไม่?</h4>
        <div class="ml-col col-6">
          <label class="form-item form-item-radio">มี
            <input type="radio" name="q9">
            <span class="checkmark"></span>
          </label>
        </div>
        <div class="ml-col col-6">
          <label class="form-item form-item-radio">ไม่มี
            <input type="radio" name="q9">
            <span class="checkmark"></span>
          </label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text" disabled required>
          <label class="label" for="unnamed">ถ้ามีโปรดระบุ</label>
        </div>
      </div>

    </div>

    <div class="profile-reg-btns">
      <a type="submit" class="btn btn-profile-reg prev"
         href="<?php echo isset($PROFILE_PAGE) ? 'profile-edit_5.php' : 'register-profile_5.php' ?>">
        กลับ
      </a>
      <button type="submit" class="btn btn-profile-reg">บันทึก</button>
      <a type="submit" class="btn btn-profile-reg next"
         href="<?php echo isset($PROFILE_PAGE) ? 'profile-edit_7.php' : 'register-profile_7.php' ?>">
        ต่อไป
      </a>
    </div>

  </form>
</div>

<script>
  $(function () {
    $('#theForm').validate({
      rules: {
        q1: { required: true },
        q2: { required: true },
        q3: { required: true },
        q4: { required: true },
        q5: { required: true },
        q6: { required: true },
        q7: { required: true },
        q8: { required: true },
        q9: { required: true },
      }
    });
  });
</script>
