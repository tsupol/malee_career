<div class="steps-and-form">
  <?php
  require_once('components/steps.php');
  get_steps(5, 'ความสามารถพิเศษ')
  ?>
  <form id="theForm" class="ml-form form-profile" action="./" method="POST">
    <h2 class="__step-title">ความสามารถด้านภาษา</h2>
    <div class="form-profile-inner">
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">ภาษา</label>
        </div>
      </div>

      <div class="ml-col col-12">
        <div class="form-item">
          <select class="ml-input" name="unnamed" id="unnamed">
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">ระดับความเข้าใจ</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <select class="ml-input" name="unnamed" id="unnamed">
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">ระดับการพูด</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <select class="ml-input" name="unnamed" id="unnamed">
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">ระดับการอ่าน</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <select class="ml-input" name="unnamed" id="unnamed">
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">ระดับการเขียน</label>
        </div>
      </div>

      <div class="ml-col col-12">
          <div class="btn">+ เพิ่มความสามารถด้านภาษาอืื่นๆ</div>
      </div>

      <h2 class="__step-title">ความสามารถด้านคอมพิวเตอร์</h2>
      <div class="ml-col col-12">
        <div class="form-item">
          <textarea class="ml-input" rows="4" cols="50" name="unnamed" id="unnamed"></textarea>
          <label class="label" for="message">อธิบายโดยสังเขป</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="btn">+ เพิ่มความสามารถด้านคอมพิวเตอร์</div>
      </div>

      <h2 class="__step-title">ความสามารถด้านอื่นๆ</h2>
      <div class="ml-col col-12">
        <div class="form-item">
          <textarea class="ml-input" rows="4" cols="50" name="unnamed" id="unnamed"></textarea>
          <label class="label" for="message">อธิบายโดยสังเขป</label>
        </div>
      </div>

      <!-- end form-->
    </div>


    <div class="profile-reg-btns">

      <a type="submit" class="btn btn-profile-reg prev"
         href="<?php echo isset($PROFILE_PAGE) ? 'profile-edit_4.php' : 'register-profile_4.php' ?>">
        กลับ
      </a>
      <button type="submit" class="btn btn-profile-reg">บันทึก</button>
      <a type="submit" class="btn btn-profile-reg next"
         href="<?php echo isset($PROFILE_PAGE) ? 'profile-edit_6.php' : 'register-profile_6.php' ?>">
        ต่อไป
      </a>
    </div>

  </form>
</div>
<script>
  $(function () {
    $('#theForm').validate();
  });
</script>
