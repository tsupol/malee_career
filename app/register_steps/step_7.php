<div class="steps-and-form">
  <?php
  require_once('components/steps.php');
  get_steps(7, 'บุคคลอ้างอิง')
  ?>
  <form id="theForm" class="ml-form form-profile" action="./" method="POST">
    <h2 class="__step-title">บุคคลอ้างอิงในกลุ่มบริษัทมาลี</h2>
    <div class="form-profile-inner">
      <!-- Question -->
      <div class="__question-wrap">
        <h4 class="__question">ท่านมีญาติหรือคนรู้จักทำงานในกลุ่มบริษัทมาลีหรือไม่?</h4>
        <div class="ml-col col-6">
          <label class="form-item form-item-radio">มี
            <input type="radio" name="q1">
            <span class="checkmark"></span>
          </label>
        </div>
        <div class="ml-col col-6">
          <label class="form-item form-item-radio">ไม่มี
            <input type="radio" name="q1">
            <span class="checkmark"></span>
          </label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text" disabled required>
          <label class="label" for="unnamed">ชื่อ-นามสกุล</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text" disabled required>
          <label class="label" for="unnamed">ตำแหน่ง</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text" disabled required>
          <label class="label" for="unnamed">ความสัมพันธ์</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <button class="btn" disabled>+ เพิ่มประวัติการอบรมหรือฝึกงาน</button>
        </div>
      </div>

      <!-- Related Person -->
      <h2 class="__step-title __margin1">บุคคลอ้างอิงที่ไม่ใช่ญาติพี่น้องหรือคู่สมรส</h2>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">ชื่อ-นามสกุล</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">ความสัมพันธ์</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">สถานที่ทำงาน/ที่อยู่</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">ตำแหน่ง</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">เบอร์โทรศัพท์</label>
        </div>
      </div>

      <hr/>

      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">ชื่อ-นามสกุล</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">ความสัมพันธ์</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">สถานที่ทำงาน/ที่อยู่</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">ตำแหน่ง</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">เบอร์โทรศัพท์</label>
        </div>
      </div>


      <!-- Emergency Contact -->
      <h2 class="__step-title __margin1">บุคคลติดต่อกรณีฉุกเฉิน</h2>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">ชื่อ-นามสกุล</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">ความสัมพันธ์</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">ที่อยู่</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">เบอร์โทรศัพท์</label>
        </div>
      </div>


      <!-- Emergency Contact -->
      <h2 class="__step-title __margin1">ท่านพร้อมจะปฏิบัติงานกับ บริษัทได้วันที่</h2>
      <div class="ml-col col-4">
        <div class="form-item">
          <select class="ml-input" name="unnamed">
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">วันที่</label>
        </div>
      </div>
      <div class="ml-col col-4">
        <div class="form-item">
          <select class="ml-input" name="unnamed">
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">เดือน</label>
        </div>
      </div>
      <div class="ml-col col-4">
        <div class="form-item">
          <select class="ml-input" name="unnamed">
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">ปี</label>
        </div>
      </div>

      <!-- Checkbox -->
      <div class="ml-col col-12">
        <label class="form-item form-item-checkbox __accept">
          ข้าพเจ้าขอรับรองว่าข้อความดังกล่าวท้ังหมดน้ีเป็นความจริงทุกประการ หากข้อความตอนหน่ึงตอนใด
          ไม่ตรงกับความเป็นจริงข้าพเจ้าขอยอมรับว่า การจ้างงานที่ตกลงนั้น เป็นอันโมฆะโดยทันที
          <input name="can_negotiate" type="checkbox">
          <span class="checkmark"></span>
        </label>
      </div>

      <!--    End form-profile-inner-->
    </div>

    <!-- Buttons -->
    <div class="profile-reg-btns">
      <a type="submit" class="btn btn-profile-reg prev"
         href="<?php echo isset($PROFILE_PAGE) ? 'profile-edit_6.php' : 'register-profile_6.php' ?>">
        กลับ
      </a>
      <button type="submit" class="btn btn-profile-reg">บันทึก</button>
      <a type="submit" class="btn btn-profile-reg next"
         href="<?php echo isset($PROFILE_PAGE) ? 'profile-edit_8.php' : 'register-profile_8.php' ?>">
        ต่อไป
      </a>
    </div>

  </form>
</div>

<script>
  $(function () {
    $('#theForm').validate({
      rules: {
        q1: { required: true },
      }
    });
  });
</script>
