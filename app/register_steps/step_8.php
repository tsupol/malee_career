  <div class="steps-and-form">
  <?php
  require_once('components/steps.php');
  get_steps(8, 'เสร็จสิ้น')
  ?>
  <form id="theForm" class="ml-form form-profile" action="./" method="POST">
    <div class="profile-summary">
      <div class="__img">
        <img src="<?php echo $asset_path ?>imgs/avatar-placeholder.jpg">
      </div>
      <h1>นาย ธราดล เทส (ดล)</h1>
      <h3>Mr. Tharadol Test (Dol)</h3>
      <p>
        <strong>ที่อยู่ปัจจุบัน</strong><br/>
        เลขที่ 14  หมู่ที่ 1  ตำบล/แขวง วัดอรุณ  อำเภอ/เขต บางกอกใหญ่  จังหวัดกรุงเทพมหานคร
        รหัสไปรษณีย์ 10600
      </p>
      <p>
        โทรศัพท์ -<br/>
        มือถือ 095 777 7777<br/>
        อีเมล mail@mail.com<br/>
      </p>
      <p>
        เกิดวันที่ 24 กันยายน 2531<br/>
        อายุ  29 ปี<br/>
        บัตรประชาชนเลขที่ 1 1018 00480 22 2<br/>
      </p>
      <p>
        ภาระทางการทหาร ได้รับการยกเว้น<br/>
        สถานภาพ โสด<br/>
        เพศ ชาย<br/>
      </p>
      <p>
        <strong>การศึกษา</strong><br/>
        มัธยมศึกษาตอนปลาย โรงเรียนเตรียมอุดมศึกษา สาขาวิชา วิทย์-คณิต<br/>
        ตั้งแต่ 2550-2553<br/>
      </p>
      <p>
        <strong>ปริญญาตรี</strong><br/>
        คณะวิศวกรรมศาสตร์ สาขาซอฟต์แวร์และความรู้ มหาวิทยาลัยเกษตรศาสตร์<br/>
        ตั้งแต่ 2554-2557<br/>
      </p>
      <h4>ประสบการณ์ทำงาน</h4>
      <ol>
        <li>บริษัท ตัวอย่าง จำกัด<br/>
          เริ่ม พ.ศ. 2557 ถึง พ.ศ. 2560 (รวมระยะเวลา 3 ปี)<br/>
          ตำแหน่ง Web Developer / ค่าจ้าง 40k<br/>
          เหตุที่ออก ต้องการเปลี่ยนงาน</li>
        <li>บริษัท ตัวอย่าง2 จำกัด<br/>
          เริ่ม พ.ศ. 2560 ถึง พ.ศ. 2561 (รวมระยะเวลา 1 ปี)<br/>
          ตำแหน่ง Web Developer / ค่าจ้าง 40k<br/>
          เหตุที่ออก ต้องการเปลี่ยนงาน</li>
      </ol>
      <p>
        <strong>ความสามารถด้านคอมพิวเตอร์</strong><br/>
        โปรแกรม Photoshop, Illustrator
      </p>
    </div>

    <!-- Buttons -->
    <div class="profile-reg-btns btns-centered">
      <a type="submit" class="btn btn-profile-reg prev"
         href="<?php echo isset($PROFILE_PAGE) ? 'profile-edit_7.php' : 'register-profile_7.php' ?>">
        กลับ
      </a>
      <div class="btn btn-profile-reg primary" id="confirm-btn">ยืนยัน</div>
    </div>

  </form>
</div>

<script>
  $(function () {
    $('#theForm').validate({
      rules: {
        q1: { required: true },
      }
    });

    // example of how to trigger the popup

    $('#confirm-btn').click(function () {
      openPopupById('popup-thank-register');
    })
  });
</script>
