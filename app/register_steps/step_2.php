<div class="steps-and-form">
  <?php
  require_once('components/steps.php');
  get_steps(2, 'การศึกษา')
  ?>
  <form id="theForm" class="ml-form form-profile" action="./" method="POST">
    <h2 class="__step-title">ประวัติการศึกษาล่าสุด</h2>
    <div class="form-profile-inner">
      <div class="ml-col col-6">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text" required>
          <label class="label" for="first_name">ระยะเวลาตั้งแต่ปี พ.ศ.</label>
        </div>
      </div>
      <div class="ml-col col-6">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text" required>
          <label class="label" for="unnamed">ถึงปี พ.ศ.</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <select class="ml-input" name="unnamed" id="unnamed" required>
            <?php include('components/select_options.php') ?>
          </select>
          <label class="label" for="unnamed">ระดับการศึกษา</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text" required>
          <label class="label" for="unnamed">สถานศึกษา</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text">
          <label class="label" for="unnamed">จังหวัดหรือประเทศ</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text" required>
          <label class="label" for="unnamed">สาขา</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="form-item">
          <input class="ml-input" name="unnamed" id="unnamed" type="text" required>
          <label class="label" for="unnamed">เกรดเฉลี่ย</label>
        </div>
      </div>
      <div class="ml-col col-12">
        <div class="btn">+ เพิ่มประวัติการศึกษา</div>
      </div>
    </div>

    <div class="profile-reg-btns">

      <a type="submit" class="btn btn-profile-reg prev"
         href="<?php echo isset($PROFILE_PAGE) ? 'profile-edit.php' : 'register-profile_1.php' ?>">
        กลับ
      </a>
      <button type="submit" class="btn btn-profile-reg">บันทึก</button>
      <a type="submit" class="btn btn-profile-reg next"
         href="<?php echo isset($PROFILE_PAGE) ? 'profile-edit_3.php' : 'register-profile_3.php' ?>">
        ต่อไป
      </a>
    </div>

  </form>
</div>
<script>
  $(function () {
    $('#theForm').validate();
  });
</script>
