<?php require_once("components/header.php"); ?>
<div class="ml-page page-forgot">
  <div class="layout-outer">
    <div class="layout-inner">
      <h1 class="heading1 centered">เปลี่ยนรหัสผ่าน</h1>
    </div>
  </div>
  <div class="layout-outer">
    <div class="layout-inner flex-center">
      <div class="layout-narrow pad-top-2">

        <form id="theForm" class="ml-form login-form" action="./" method="POST">
          <div class="clearfix">
            <div class="sm-col col-12 px2">
              <div class="form-item">
                <input class="ml-input" name="password" type="password" id="password">
                <label class="label" for="password">กรอกรหัสผ่านใหม่</label>
                <label class="caption" for="password">กรุณากรอกรหัสผ่านมากกว่า 6 ตัวอักษร</label>
              </div>
            </div>
            <div class="sm-col col-12 px2">
              <div class="form-item">
                <input class="ml-input" name="confirm_password" type="password">
                <label class="label" for="confirm_password">ยืนยันรหัสผ่านใหม่</label>
              </div>
            </div>
            <div class="sm-col col-12 px2">
              <button type="submit" class="btn btn-submit no-margin">ยืนยัน</button>
            </div>
          </div>
        </form>

        <div class="sm-col col-12 px2 bottom-message">
          <a href="#" class="ml-link">ยกเลิก</a>
        </div>

      </div>
    </div>
  </div>
</div>
<script>
  $(function () {
    $('#theForm').validate({
      rules: {
        password: {
          required: true,
          minlength: 6,
        },
        confirm_password: {
          equalTo: '#password',
        }
      },
      messages: {
        password: {
          required: 'กรุณากรอกรหัสผ่านมากกว่า 6 ตัวอักษร',
          minlength: 'กรุณากรอกรหัสผ่านมากกว่า 6 ตัวอักษร',
        },
        confirm_password: {
          equalTo: 'รหัสผ่านไม่ตรงกัน',
        }
      },
    });
  });
</script>

<?php require_once("components/footer.php"); ?>
