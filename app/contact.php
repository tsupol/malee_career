<?php
require_once("components/header.php");
require_once("components/social_share.php");
?>
<div class="ml-page page-contact">
  <div class="sec-banner-2 banner-contact">
    <div>
      <h1>
        As a company, Malee Group must continue<br/>
        to grow and become stronger<br class="mdm-only"> so that we can<br/>
        in turn take care of more.<br/><br/>

        Because by<br class="mdm-only"/>
        <span class="color-danger">“Growing Well Together”</span>,
        <br class="mdm-only"/> we will<br class="md-only"/>
        all prosper together.
      </h1>
      <h3>
        Mr. Chatchai Boonyarat<br/>
        Chairman of the Board of Directors<br/>
        Malee Group Public Company Limited
      </h3>
    </div>
  </div>
  <div class="layout-outer">
    <div class="layout-inner">
      <ul class="breadcrumb">
        <li><a href="index.php">หน้าแรก</a></li>
        <li><a class="active">ติดต่อ HR</a></li>
      </ul>
      <h1 class="heading1 centered primary">ติดต่อ HR</h1>

      <form id="theForm" class="ml-form contact-form" action="./" method="POST">
        <div class="clearfix">

          <div class="sm-col col-12 px2">
            <div class="form-item">
              <input class="ml-input" name="subject" id="subject" type="text">
              <label class="label" for="subject">Subject</label>
            </div>
          </div>
          <div class="sm-col col-12 px2">
            <div class="form-item">
              <input class="ml-input" name="name" id="name" type="text">
              <label class="label" for="name">Name</label>
            </div>
          </div>
          <div class="sm-col col-12 px2">
            <div class="form-item">
              <input class="ml-input" name="email" id="email" type="text">
              <label class="label" for="email">Email</label>
            </div>
          </div>
          <div class="sm-col col-12 px2">
            <div class="form-item">
              <input class="ml-input" name="province" id="province" type="text">
              <label class="label" for="province">Province</label>
            </div>
          </div>
          <div class="sm-col col-12 px2">
            <div class="form-item">
              <input class="ml-input" name="telephone" id="telephone" type="text">
              <label class="label" for="telephone">Telephone</label>
            </div>
          </div>
          <div class="sm-col col-12 px2">
            <div class="form-item">
              <textarea class="ml-input" rows="4" cols="50" name="message" id="message"></textarea>
              <label class="label" for="message">Message</label>
            </div>
          </div>

          <div class="sm-col col-12 px2 font2 form-info">
            <i class="ico-info fas fa-info-circle"></i>All fields are required.
          </div>

          <div class="sm-col col-12 px2 contact-btns">
            <button type="submit" class="btn btn-submit primary">ส่งข้อมูล</button>
            <div class="btn btn-submit" id="clear-btn">ลบข้อมูล</div>
          </div>
        </div>
      </form>

      <!-- Maps -->

      <div class="map-big map">
        <div class="__img">
          <img src="<?php echo $asset_path ?>imgs/map-1.jpg"/>
        </div>
        <div class="__desc">
          <p class="__title">Malee Recruitment Center</p>
          <p class="__address">401/1 ถนนพหลโยธิน เขตลำลูกกา จังหวัดปทุมธานี 12130 </p>
          <a class="__gmap" href="#">เปิด Google Map</a>
          <a class="__phone" href="#">+66(2) 992 5800</a>
          <a class="__email" href="#">recruit@malee.co.th</a>
          <div class="socials">
            <a href="#"><i class="fab fa-facebook-f"></i></a>
            <a href="#"><i class="fab fa-linkedin"></i></a>
          </div>
        </div>
      </div>

      <!-- Layout Inner-->
    </div>
  </div>
  <div class="layout-outer bg1">
    <div class="layout-inner">
      <!-- Mini Maps -->
      <h2 class="heading-places">สถานที่ทำงาน</h2>
      <div class="places">

        <div class="map">
          <div class="__img">
            <img src="<?php echo $asset_path ?>imgs/map-2.jpg"/>
          </div>
          <div class="__desc">
            <p class="__title">บริษัท มาลีกรุ๊ป จำกัด (มหาชน)</p>
            <p class="__subtitle">สำนักงานรังสิต</p>
            <p class="__address">401/1 ถนนพหลโยธิน เขตลำลูกกา จังหวัดปทุมธานี 12130</p>
            <a class="__gmap" href="#">เปิด Google Map</a>
            <a class="__phone" href="#">+66(2) 080 7899</a>
            <a class="__email" href="#">callcenter@malee.co.th</a>
          </div>
        </div>
        <div class="map">
          <div class="__img">
            <img src="<?php echo $asset_path ?>imgs/map-3.jpg"/>
          </div>
          <div class="__desc">
            <p class="__title">บริษัท มาลีกรุ๊ป จำกัด (มหาชน)</p>
            <p class="__subtitle">สำนักงานสามพราน</p>
            <p class="__address">26/1 ถนนทางเข้าอำเภอสามพราน ตำบลยายชา<br/>อำเภอสามพราน จังหวัดนครปฐม 73110</p>
            <a class="__gmap" href="#">เปิด Google Map</a>
            <a class="__phone" href="#">+66(34) 311 310</a>
            <a class="__email" href="#">callcenter@malee.co.th</a>
          </div>
        </div>
        <div class="map">
          <div class="__img">
            <img src="<?php echo $asset_path ?>imgs/map-4.jpg"/>
          </div>
          <div class="__desc">
            <p class="__title">บริษัท มาลีเอ็นเตอร์ไพรส์ จํากัด</p>
            <p class="__subtitle">สำนักงานรังสิต</p>
            <p class="__address">401/1 ถนนพหลโยธิน เขตลำลูกกา จังหวัดปทุมธานี 12130</p>
            <a class="__gmap" href="#">เปิด Google Map</a>
            <a class="__phone" href="#">+66(2) 080 7899</a>
            <a class="__email" href="#">callcenter@malee.co.th</a>
          </div>
        </div>

      </div>
    </div>
  </div>

</div>

<script>
  $(function () {
    var validator = $('#theForm').validate({
      rules: {
        subject: { required: true },
        name: { required: true },
        email: { required: true },
        province: { required: true },
        telephone: { required: true },
        message: { required: true },
      },
      messages: {
        subject: { required: 'This field is required' },
        name: { required: 'This field is required' },
        email: { required: 'This field is required' },
        province: { required: 'This field is required' },
        telephone: { required: 'This field is required' },
        message: { required: 'This field is required' },
      }
    });
    $('#clear-btn').click(function () {
      $('.ml-input').val('').parent().removeClass('has-value');
      validator.resetForm();
    })
  });
</script>

<?php require_once("components/footer.php"); ?>
