<?php require_once("components/header.php"); ?>
<div class="ml-page page-profile">
  <div class="sec-banner-2"></div>
  <div class="layout-outer">
    <div class="layout-inner">
      <div class="layout-profile-page">

        <?php
        require_once('components/profile_nav.php');
        get_profile_nav(3)
        ?>

        <div class="sec-content">
          <h1 class="heading1">ภาพรวมบัญชีผู้ใช้</h1>
          <!-- Card 1-->
          <div class="card-profile">
            <div class="cp-inner">
              <h1>ประวัติการสมัครงาน</h1>
              <div class="pp-job-list history-page">

                <?php for ($i = 0; $i < 10; $i++) { ?>
                  <div class="__row">
                    <div class="__col col-title">
                      <p class="__title"><span class="bold">Software Developer / Programmer</span></p>
                      <p class="__date">24/05/2561</p>
                    </div>
                    <div class="__col col-company">
                      <p class="__company">บริษัท มาลีเอ็นเตอร์ไพรส์ จํากัด</p>
                      <p class="__company-branch">สำนักงานรังสิต</p>
                    </div>
                    <div class="__col col-print">
                      <a class="btn-underline" href="#"><i class="fas fa-print"></i>พิมพ์ใบสมัคร</a>
                    </div>
                  </div>
                <?php } ?>

                <?php include('components/pagination.php') ?>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $(function () {
    $('#theForm').validate();
  });
</script>

<?php require_once("components/footer.php"); ?>
