<?php require_once("components/header.php"); ?>
<div class="ml-page page-forgot page-thank-you">
  <div class="layout-outer">
    <div class="layout-inner">
      <h1 class="heading1 centered">THANK YOU</h1>
      <h3 class="heading3 centered sm-only">
        Thank you for your interest in joining us.<br/>
        Please note that we will get in touch with shortlisted candidates only.<br/>
        If you do not hear back from us within a month, it may automatically imply<br/>
        that you are not selected this time, however, your resume will be kept for future consideration.
      </h3>
      <h3 class="heading3 centered smm-only">
        Thank you for your interest in joining us.<br/>
        Please note that we will get in touch with shortlisted<br/>
        candidates only. If you do not hear back from us<br/>
        within a month, it may automatically imply<br/>
        that you are not selected this time,<br/>
        however, your resume will be kept<br/>
        for future consideration.
      </h3>
    </div>
  </div>
  <div class="layout-outer">
    <div class="layout-inner flex-center">
      <div class="layout-narrow pad-top-2">

        <div class="clearfix">
          <div class="sm-col col-12 px2">
            <a href="index.php" type="submit" class="btn btn-back-home">กลับสู่หน้าหลัก</a>
          </div>
        </div>

        <div class="sm-col col-12 px2 bottom-message">
          ถ้าคุณต้องการความช่วยเหลือ กรุณา <a href="contact.php" class="ml-link underline bold">ติดต่อ HR</a>
        </div>

      </div>
    </div>
  </div>
</div>
<script>
  $(function () {
    $('#theForm').validate({
      rules: {
        email: {
          required: true,
        },
        password: {
          required: true,
        }
      },
    });
  });
</script>

<?php require_once("components/footer.php"); ?>
