<?php require_once("components/header.php"); ?>
<div class="ml-page page-apply-job">
  <div class="sec-banner"></div>
  <div class="layout-outer">
    <div class="layout-inner">
      <ul class="breadcrumb">
        <li><a href="index.php">หน้าแรก</a></li>
        <li><a href="#">ร่วมงานกับมาลี</a></li>
        <li><a href="#">Web Developer</a></li>
        <li><a href="#" class="active">สมัครงาน</a></li>
      </ul>
      <h1 class="heading1 centered">สมัครงาน</h1>
      <div class="wws-content-wrap">

        <div class="profile-info">
          <div class="__left">
            <img src="<?php echo $asset_path ?>imgs/avatar-placeholder.jpg">
          </div>
          <div class="__right">
            <div class="ml-px2">
              <h1>Tharadorl Test (Dol)</h1>
              <div class="__row">
                <div class="__col __label">ตำแหน่งงานล่าสุด</div>
                <div class="__col __desc">Project Manager</div>
              </div>
              <div class="__row">
                <div class="__col __label">การศึกษา</div>
                <div class="__col __desc">ปริญญาตรี คณะวิศวกรรมศาสตร์ สาขาซอฟต์แวร์และความรู้ มหาวิทยาลัยเกษตรศาสตร์</div>
              </div>
              <div class="__row">
                <div class="__col __label">เบอร์ติดต่อ</div>
                <div class="__col __desc">095 777 7777</div>
              </div>
              <div class="__row">
                <div class="__col __label">อีเมล</div>
                <div class="__col __desc">mail@mail.com</div>
              </div>
            </div>
            <!--            <select name="source">-->
            <!--              <option value="1">Audi</option>-->
            <!--              <option value="2">BMW</option>-->
            <!--              <option value="3">Citroen</option>-->
            <!--            </select>-->
            <div class="ml-form-wrap">
              <form id="theForm" class="clearfix" action="./" method="POST">
                <div class="sm-col col-12 px2">
                  <p class="form-caption">
                    กรุณากรอกข้อมูลของท่านให้ถูกต้อง ครบถ้วนสมบูรณ์
                    กรุณากรอกข้อมูลที่มีเครื่องหมาย * ให้ครบถ้วน
                  </p>
                </div>

                <div class="sm-col col-12 px2">
                  <div class="form-item">
                    <label for="source">ทราบข่าวสารการรับสมัครจาก*</label>
                    <select class="ml-input" name="source">
                      <option class="hidden" label=" "></option>
                      <option value="1">Audi</option>
                      <option value="2">BMW</option>
                      <option value="3">Citroen</option>
                    </select>
                  </div>
                </div>

                <div class="sm-col col-12 px2">
                  <div class="form-item">
                    <label for="number">ที่อื่นโปรดระบุ</label>
                    <input class="ml-input" name="number" type="text">
                  </div>
                </div>


                <div class="sm-col col-4 px2">
                  <div class="form-item">
                    <label for="number">ที่อื่นโปรดระบุ</label>
                    <input class="ml-input" name="number" type="text">
                    <label class="caption" for="source">กรุณากรอกข้อมูล1</label>
                  </div>
                </div>
                <div class="sm-col col-4 px2">
                  <div class="form-item">
                    <label for="number">ที่อื่นโปรดระบุ</label>
                    <input class="ml-input" name="number" type="text">
                  </div>
                </div>
                <div class="sm-col col-4 px2">
                  <div class="form-item">
                    <label for="number">ที่อื่นโปรดระบุ</label>
                    <input class="ml-input" name="number" type="text">
                  </div>
                </div>


                <div class="sm-col col-12 px2">
                  <div class="form-item">
                    <label for="start-date">เริ่มงานได้วันที่</label>
                    <input class="ml-input" data-toggle="datepicker" name="start-date">
                    <div class="icon-wrap fa-wrap">
                      <i class="fas fa-calendar-alt"></i>
                    </div>
                  </div>
                </div>
                <div class="sm-col col-12 px2">
                  <label class="form-item form-item-checkbox">สามารถต่อรองเงินได้
                    <input type="checkbox" checked="checked">
                    <span class="checkmark"></span>
                  </label>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
      <!--      <div class="wws-content">-->
      <!--              <form id="theForm" action="./" method="POST">-->
      <!--      -->
      <!--              </form>-->
      <!--      </div>-->

      <div class=test-chevron">
        <div class="chevron"></div>
      </div>
    </div>
  </div>
</div>

<script>
  $(function () {

    $('#theForm').validate({
      messages: {
        sources: {
          required: "กรุณากรอกข้อมูล [your message here]",
        }
      },
      submitHandler: function(form) {
        console.log('form', $(form).serialize());
      }
      // lang: 'th'
    });
  });
</script>

<?php require_once("components/footer.php"); ?>
