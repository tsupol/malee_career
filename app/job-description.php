<?php
require_once("components/header.php");
require_once("components/social_share.php");
?>
<div class="ml-page page-job-desc">
  <div class="sec-banner"></div>
  <div class="layout-outer">
    <div class="layout-inner">

      <ul class="breadcrumb">
        <li><a href="index.php">หน้าแรก</a></li>
        <li><a href="work-with-us.php">ร่วมงานกับมาลี</a></li>
        <li class="active">Product Manager</li>
      </ul>
      <h1 class="heading1 centered primary">Product Manager</h1>
      <h3 class="heading3 centered primary">บริษัท มาลีเอ็นเตอร์ไพรส์ จํากัด</h3>
      <h4 class="head-branch">(สำนักงานรังสิต)</h4>
      <h4 class="head-date">22/05/2561</h4>

      <div class="head-btns">
        <a href="work-with-us" class="btn primary">สมัครงาน</a>
        <a href="#" class="btn btn-outline">สมัครงานด้วย LinkedIn</a>
      </div>

      <div class="jd-inner">
        <div class="jd-content">
          <h4>รายละเอียดตำแหน่งงาน</h4>
          <div class="__section">
            <h5>Roles & Responsibilities:</h5>
            <ul>
              <li>
                Reporting to International Marketing Head, and you will be working in partnership with various teams/agencies to assist in defining social
                media and digital media strategy for the APAC, especially CLMV and Greater China region with feasible expansion of responsibilities into other
                region as business expands in the near future.
              </li>
              <li>You will be responsible for initiating and driving Malee’s social media and digital marketing strategies to build digital capabilities and brand
                awareness in the region.
              </li>
              <li>Planning, budgeting, implementing, and evaluating social media and digital tactical actions in order to increase brand awareness and drive
                new potential customers/traffics.
              </li>
              <li>Organizing and attending events, such as conferences, seminars, and exhibitions.</li>
            </ul>
          </div>
          <div class="__section">
            <h5>Qualifications</h5>
            <p>We are seeking a unique digital/social marketer to join our growing international marketing team. This position offers excellent career prospects for
              person with a deep knowledge and strong experience.</p>
            <ul>
              <li>Bachelor degree in Marketing, Business or a related field</li>
              <li>At least 5 years’ experience in brand/product management</li>
              <li>Good analytical, creative, initiative, pro-active, self-motivated and self-starter</li>
              <li>Strong team player and communication skills</li>
              <li>Good command of English and computer literacy</li>
            </ul>
          </div>
          <div class="__section">
            <h5>Strategic Marketing:</h5>
            <ul>
              <li>Participate in strategic planning to formulate the company&#39;s future direction</li>
              <li>Align product strategy to the company&#39;s business strategy</li>
              <li>Understand changing market dynamics and translate them into actionable strategy</li>
              <li>Identify levers that will drive successful marketing strategy and quantify the impact of spending on the success</li>
              <li>Develop and communicate marketing plan of assigned products</li>
            </ul>
          </div>
          <div class="__section">
            <h5>Define Product strategy and manage brands:</h5>
            <ul>
              <li>Participate in strategic planning to formulate the company&#39;s future direction</li>
              <li>Align product strategy to the company&#39;s business strategy</li>
              <li>Understand changing market dynamics and translate them into actionable strategy</li>
              <li>Identify levers that will drive successful marketing strategy and quantify the impact of spending on the success</li>
              <li>Develop and communicate marketing plan of assigned products</li>
            </ul>
          </div>
          <div class="jd-2col">
            <div class="__row">
              <div class="__col col-label">จำนวนอัตรา:</div>
              <div class="__col col-desc">3</div>
            </div>
            <div class="__row">
              <div class="__col col-label">Qualification:</div>
              <div class="__col col-desc">Bachelor Degree</div>
            </div>
            <div class="__row">
              <div class="__col col-label">ประเภทกลุ่มงาน</div>
              <div class="__col col-desc">Brand / Product Management</div>
            </div>
            <div class="__row">
              <div class="__col col-label">สถานที่ปฏิบัติงาน</div>
              <div class="__col col-desc col-location">
                บริษัท มาลี กรุ๊ป จำกัด (สำนักงานรังสิต)<br/>
                401/1 หมู่ 8 ถนนพหลโยธิน ลำลูกกา จังหวัดปทุมธานี 12130<br/>
                <a class="gmap-link" href="https://goo.gl/maps/UWeoxnsNYz42" target="_blank">เปิด Google map</a>
                <br/>
                <img class="map" src="<?php echo $asset_path ?>imgs/map-1.jpg"/>
              </div>
            </div>
            <div class="__row">
              <div class="__col col-label">Benefits</div>
              <ul class="benefits">
                <li>Five-day work week</li>
                <li>Group Accident Insurance</li>
                <li>Medical Expense Reimbursement</li>
                <li>Uniform</li>
                <li>Performance Bonus</li>
                <li>Car Allowance</li>
                <li>Mileague Allowance</li>
                <li>Shift Allowance</li>
                <li>Diligent Incentive</li>
                <li>Annual Leave</li>
                <li>Travel Per diem</li>
                <li>Personal Leave with Pay</li>
                <li>Traditional Holiday</li>
              </ul>
            </div>
          </div>
        </div>
      </div>


      <div class="head-btns at-bottom">
        <a href="work-with-us" class="btn primary">สมัครงาน</a>
        <a href="#" class="btn btn-outline">สมัครงานด้วย LinkedIn</a>
      </div>

      <!--      layout inner-->
    </div>
  </div>
</div>

<script>
  $(function () {
    $('#theForm').validate();
  });
</script>

<?php require_once("components/footer.php"); ?>
