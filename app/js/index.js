$(function () {

  mlInitMenu();
  mlInitForm();
  mlInitWidgets();
  initAllPopups();
  navSetActive();
  centerActivityMenu();

});

function mlInitMenu () {

  // Nav-Activity (Mega Menu)
  // ----------------------------------------

  $('#nav-activity-item').click(function () {
    $('#nav-activity').toggleClass('show');
    event.stopPropagation();
  });
  // close on click outside
  $('#nav-activity').click(function () {
    event.stopPropagation();
  });

  // Mobile Menu - profile
  // ----------------------------------------

  $('.logged-in-m').click(function (event) {
    $('.mm-profile').toggleClass('show');
    event.stopPropagation();
  });

  if (mobileAndTabletcheck()) {
    $(document).on("touchstart", '.mm-profile', function (event) {
      if ($(this).hasClass('expanded')) {
        var me = $(this);
        setTimeout(function () {
          me.removeClass('expanded');
        }, 200);
      } else {
        $(this).addClass('expanded');
      }

      event.stopPropagation();
    });
  } else {
    $(document).on("click", '.mm-profile', function (event) {
      $(this).toggleClass('expanded');
      event.stopPropagation();
    });
  }

  if (mobileAndTabletcheck()) {
    $(document).on("touchend", '.ml-page ', function (e) {
      $('.hide-click-outside.show').removeClass('show');
      event.stopPropagation();
    })
  } else {
    $(document).on("click", '.ml-page', function (e) {
      $('.hide-click-outside.show').removeClass('show');
    });
  }

  // Mobile Menu
  // ----------------------------------------

  $('.hamburger').click(function () {
    $(this).toggleClass('change');
    if ($(this).hasClass('change')) {
      document.body.classList.add('mobile-menu-expanded');
    } else {
      document.body.classList.remove('mobile-menu-expanded');
    }
  });

  // Animation (Accordion - auto height)
  // Note: very complex just for a simple thing
  // ----------------------------------------

  var mmContent = $('.mm-content-wrap');
  var mmWrap = $('.mobile-menu');
  mmContent.css('display', 'inline-block'); // for saving the accordion height
  mmWrap.css('display', 'inline-block'); // for saving the accordion height
  var mmAccDrop = $('.mm-accordion .nested-wrap');
  var mmAccStay = $('.mm-accordion .expandable');
  mmAccDrop.data('height', mmAccDrop.height());
  // console.log('el.height()', mmAccDrop.height());

  mmAccStay.click(function () {
    mmAccStay.toggleClass('expanded');
    if (mmAccStay.hasClass('expanded')) {
      mmAccDrop.css('height', mmAccDrop.data('height') + 'px')
    } else {
      mmAccDrop.css('height', 0)
    }
  });
  mmAccStay.removeClass('expanded');
  mmAccDrop.css('height', 0);
  mmContent.css('display', ''); // hide again
  mmWrap.css('display', ''); // hide again

}

function mlInitForm () {
  // Malee Form
  // ----------------------------------------

  $('.ml-input').focus(function () {
    $(this).parent().addClass('focus')
  }).blur(function () {
    $(this).parent().removeClass('focus')
  }).change(function () {
    if (this.value === '') {
      $(this).parent().removeClass('has-value')
    } else {
      $(this).parent().addClass('has-value')
    }
  });

  // append chevron
  $('select.ml-input').after('<div class="icon-wrap"><div class="chevron"></div></div>');

  // Check for initial value
  $('.ml-input').each(function () {
    if ($(this).val() != '') {
      $(this).parent().addClass('has-value')
    }
  });

  /**
   * Note: this will auto append red * to the required field
   */
  $('.ml-input[required]').siblings('label:not(.error)').append('<span class="color-danger">*</span>');

  /**
   * Note: Change default validation error message here
   */
  jQuery.extend(jQuery.validator.messages, {
    required: "กรุณากรอกข้อมูล",
    remote: "Please fix this field.",
    email: "Please enter a valid email address.",
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Please enter a valid number.",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Please enter the same value again.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    minlength: jQuery.validator.format("Please enter at least {0} characters."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
  });

  // DatePicker
  // ----------------------------------------

  $('[data-toggle="datepicker"]').datepicker({
    // format: 'yyyy-mm-dd'
    format: 'dd-mm-yyyy'
  });

  // Table: Pagination
  // ----------------------------------------
  $('.pagination .pg-select').each(function () {
    $(this)
      .wrap('<div class="pg-select-wrap"></div>')
      .after('<div class="chevron-wrap"><div class="chevron slim"></div></div>')
  });
}

function mlInitWidgets () {

  // Back to top
  // ----------------------------------------
  $('.back-to-top').click(function () {
    $("html, body").animate({ scrollTop: 0 }, 500);
  });

  window.onscroll = function () {
    scrollFunction()
  };

  function scrollFunction () {
    /**
     * Note: should be enough to make the navbar appear sticky
     * (140 - 71px)
     */
    if (document.body.scrollTop > 69 || document.documentElement.scrollTop > 69) {
      document.body.classList.add('scrolled');
      $('.back-to-top').removeClass('hidden')
    } else {
      document.body.classList.remove('scrolled');
      $('.back-to-top').addClass('hidden')
    }
  }

  // Mobile Share
  // ----------------------------------------
  $('.smm-share-icon').click(function () {
    $(this).parents('.social-share-mobile').toggleClass('show');
    event.stopPropagation();
  });
}

function navSetActive () {
  var urlSegments = window.location.href.split('/');
  var currentPage = urlSegments[urlSegments.length - 1].replace('#', '').replace('.php', '').trim();
  if(currentPage === '') {
    currentPage = 'index'
  }

  /**
   * Note: CNC testers don't want mobile menu to be active for some reasons.
   */
  // var $menus = $('.main-menu, .mobile-menu');
  var $menus = $('.main-menu');

  // Main Nav (both mobile and desktop)
  $menus.find('[data-active]').each(function () {
    var href = $(this).attr('data-active');
    if(href === currentPage) {
      $(this).addClass('active')
        // .parents('[data-active]').addClass('active') // Note: use regex instead (for content pages)
    }
  });
  // `activity` page contains many sub-pages
  if(/activity/.test(currentPage)) {
    $('#nav-activity-item').addClass('active')
  }
  if(/job-description/.test(currentPage)) {
    $menus.find('[data-active=work-with-us]').addClass('active')
  }
  // Activity Page
  $('.act-nav').find('[data-active]').each(function () {
    var href = $(this).attr('data-active');
    if(href === currentPage) {
      $(this).addClass('active')
    }
  });
}

function centerActivityMenu () {
  var $menu = $('.act-nav ._inner').first();
  var winWidth = window.innerWidth;
  if($menu.length && winWidth < 800) {
    var $active = $menu.find('.active');
    var left = $active.offset().left;
    var width = $active.width();
    $menu.scrollLeft(left - (winWidth/2) + (width/2));
  }
}


function mobileAndTabletcheck () {
  var check = false;
  (function (a) {
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
  })(navigator.userAgent || navigator.vendor || window.opera);
  return check;
}
