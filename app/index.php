<?php
require_once("components/header.php");
require_once ("components/social_share.php");
?>


<div class="ml-page page-home">
  <div class="sec-banner-home">
    <div class="layout-inner no-padding">
      <div class="banner-content">
        <div class="__content">
          <h1>ยินดีต้อนรับ</h1>
          <h2>เพื่อให้คุณได้ใช้ชีวิตในแบบที่คุณเลือก<br/>และเติบโตไปพร้อมกับเรา</h2>
          <a href="#" target="_blank" class="btn-outline-d">กดเพื่อดูวิดีโอ</a>
        </div>
      </div>
    </div>
  </div>

  <div class="layout-outer home-ref-bg">
    <div class="layout-inner">
      <h1 class="heading1 centered primary">ยินดีต้อนรับสู่ครอบครัวมาลี</h1>
      <h2 class="heading3 centered primary">
        เราพร้อมตอบรับทุกความต้องการของชีวิต เพื่อให้คุณได้ใช้ชีวิตในแบบที่คุณเลือก และเติบโตไปพร้อมกับเรา
      </h2>

      <?php
      require_once('components/job_filter_table.php');
      get_job_filter_table(false)
      ?>

    </div>
  </div>
  <!-- Home Grow Together-->
  <div class="home-content layout-outer">
    <div class="layout-inner">
      <div class="grow-together">
        <img src="<?php echo $asset_path ?>imgs/logo-malee.png"/>
        <div class="gt-text">
          <h1>“โตไปด้วยกัน”</h1>
          <h3>คุณค่าที่มาลีตั้งใจมอบให้</h3>
        </div>
      </div>

      <div class="home-banners">
        <a href="#" class="banner banner-1">
          <div class="banner-ov">
            <img src="<?php echo $asset_path ?>imgs/home/play-icon.png"/>
          </div>
        </a>
        <div class="right-banners">
          <a href="#" class="banner banner-2">
            <div class="banner-ov">
              <h1>+12</h1>
              <h3>สวัสดิการ</h3>
            </div>
          </a>
          <a href="#" class="banner banner-3">
            <div class="banner-ov">
              <h1>+9</h1>
              <h3>สิ่งอำนวยความสะดวก</h3>
            </div>
          </a>
        </div>
      </div>

      <div class="home-activities">
        <a href="#" class="banner act-1">
          <div class="banner-ov">
            <div class="act-text">
              <h2>กิจกรรมครอบครัวมาลี</h2>
              <div class="btn-outline-d inverse btn-activity">ดูทั้งหมด</div>
            </div>
          </div>
        </a>
        <a href="#" class="banner act-2">
          <div class="banner-ov">
            <div class="act-text">
              <h2>กิจกรรมพัฒนาต่างๆ</h2>
              <div class="btn-outline-d inverse btn-activity">ดูทั้งหมด</div>
            </div>
          </div>
        </a>
        <a href="#" class="banner act-3">
          <div class="banner-ov">
            <div class="act-text">
              <h2>กิจกรรม CSR</h2>
              <div class="btn-outline-d inverse btn-activity">ดูทั้งหมด</div>
            </div>
          </div>
        </a>
        <a href="#" class="banner act-4">
          <div class="banner-ov">
            <div class="act-text">
              <h2>กิจกรรมโครงการร่วมกับสถาบัน<br/>
                การศึกษาภายนอก</h2>
              <div class="btn-outline-d inverse btn-activity">ดูทั้งหมด</div>
            </div>
          </div>
        </a>
      </div>

    </div>
  </div>
</div>

<script>
  $(function () {
  });
</script>

<?php require_once("components/footer.php"); ?>
