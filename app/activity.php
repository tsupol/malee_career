<?php
require_once("components/header.php");
require_once("components/social_share.php");
?>
<div class="ml-page page-activity ml-normalize">
  <div class="sec-banner"></div>
  <div class="layout-outer">
    <div class="layout-inner">
      <ul class="breadcrumb">
        <li><a href="index.php">หน้าแรก</a></li>
        <li><a class="active">กิจกรรมในครอบครัว</a></li>
      </ul>
      <h1 class="heading1 centered">กิจกรรมในครอบครัว</h1>

      <!-- Nav -->
      <div class="act-nav">
        <ul class="_inner">
          <li data-active="activity"><a href="activity.php">ทั้งหมด</a></li>
          <li data-active="activity-family"><a href="activity-family.php">กิจกรรมครอบครัวมาลี</a></li>
          <li data-active="activity-develop"><a href="activity-develop.php">กิจกรรมพัฒนาต่างๆ</a></li>
          <li data-active="activity-csr"><a href="activity-csr.php">กิจกรรม CSR</a></li>
          <li data-active="activity-joint"><a href="activity-joint.php">กิจกรรมร่วมกับสถาบันภายนอก</a></li>
          <li data-active="" class="mobile-pad">-</li>
        </ul>
      </div>

      <!-- Highlight Section -->
      <div class="act-sec">

        <div class="act-item col-12 xs-col-6">
          <div class="_padding">
            <div class="_inner">
              <a href="activity-content.php" class="_img">
                <img src="<?php echo $asset_path ?>imgs/activity/act-1.jpg"/>
              </a>
              <div class="_content">
                <p class="__title">
                  <a href="activity-content.php">โครงการประกอบการพบนักศึกษา / คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง</a>
                </p>
                <p class="__desc">คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง
                  เข้าร่วมโครงการสถานประกอบการพบนักศึกษา ณ คณะวิศวกรรมศาสตร์
                </p>
                <p class="__link"><a href="#">อ่านต่อ</a></p>
              </div>
            </div>
          </div>
        </div>

        <div class="act-item col-12 xs-col-6">
          <div class="_padding">
            <div class="_inner">
              <a href="activity-content.php" class="_img">
                <img src="<?php echo $asset_path ?>imgs/activity/act-2.jpg"/>
              </a>
              <div class="_content">
                <p class="__title">
                  <a href="activity-content.php">โครงการประกอบการพบนักศึกษา / คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง</a>
                </p>
                <p class="__desc">คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง
                  เข้าร่วมโครงการสถานประกอบการพบนักศึกษา ณ คณะวิศวกรรมศาสตร์
                </p>
                <p class="__link"><a href="#">อ่านต่อ</a></p>
              </div>
            </div>
          </div>
        </div>

      </div>

      <!-- List Section -->
      <div class="act-sec act-sec-list">

        <div class="act-item col-12 xs-col-6 md-col-4">
          <div class="_padding">
            <div class="_inner">
              <a href="activity-content.php" class="_img">
                <img src="<?php echo $asset_path ?>imgs/activity/act-3.jpg"/>
              </a>
              <div class="_content">
                <p class="__title"><a href="activity-content.php">โครงการประกอบการพบนักศึกษา / คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง</a></p>
                <p class="__desc">คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง
                  เข้าร่วมโครงการสถานประกอบการพบนักศึกษา ณ คณะวิศวกรรมศาสตร์</p>
                <p class="__link"><a href="#">อ่านต่อ</a></p>
              </div>
            </div>
          </div>
        </div>
        <div class="act-item col-12 xs-col-6 md-col-4">
          <div class="_padding">
            <div class="_inner">
              <a href="activity-content.php" class="_img">
                <img src="<?php echo $asset_path ?>imgs/activity/act-4.jpg"/>
              </a>
              <div class="_content">
                <p class="__title"><a href="activity-content.php">โครงการประกอบการพบนักศึกษา / คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง</a></p>
                <p class="__desc">คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง
                  เข้าร่วมโครงการสถานประกอบการพบนักศึกษา ณ คณะวิศวกรรมศาสตร์</p>
                <p class="__link"><a href="#">อ่านต่อ</a></p>
              </div>
            </div>
          </div>
        </div>
        <div class="act-item col-12 xs-col-6 md-col-4">
          <div class="_padding">
            <div class="_inner">
              <a href="activity-content.php" class="_img">
                <img src="<?php echo $asset_path ?>imgs/activity/act-5.jpg"/>
              </a>
              <div class="_content">
                <p class="__title"><a href="activity-content.php">โครงการประกอบการพบนักศึกษา / คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง</a></p>
                <p class="__desc">คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง
                  เข้าร่วมโครงการสถานประกอบการพบนักศึกษา ณ คณะวิศวกรรมศาสตร์</p>
                <p class="__link"><a href="#">อ่านต่อ</a></p>
              </div>
            </div>
          </div>
        </div>
        <div class="act-item col-12 xs-col-6 md-col-4">
          <div class="_padding">
            <div class="_inner">
              <a href="activity-content.php" class="_img">
                <img src="<?php echo $asset_path ?>imgs/activity/act-6.jpg"/>
              </a>
              <div class="_content">
                <p class="__title"><a href="activity-content.php">โครงการประกอบการพบนักศึกษา / คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง</a></p>
                <p class="__desc">คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง
                  เข้าร่วมโครงการสถานประกอบการพบนักศึกษา ณ คณะวิศวกรรมศาสตร์</p>
                <p class="__link"><a href="#">อ่านต่อ</a></p>
              </div>
            </div>
          </div>
        </div>
        <div class="act-item col-12 xs-col-6 md-col-4">
          <div class="_padding">
            <div class="_inner">
              <a href="activity-content.php" class="_img">
                <img src="<?php echo $asset_path ?>imgs/activity/act-7.jpg"/>
              </a>
              <div class="_content">
                <p class="__title"><a href="activity-content.php">โครงการประกอบการพบนักศึกษา / คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง</a></p>
                <p class="__desc">คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง
                  เข้าร่วมโครงการสถานประกอบการพบนักศึกษา ณ คณะวิศวกรรมศาสตร์</p>
                <p class="__link"><a href="#">อ่านต่อ</a></p>
              </div>
            </div>
          </div>
        </div>
        <div class="act-item col-12 xs-col-6 md-col-4">
          <div class="_padding">
            <div class="_inner">
              <a href="activity-content.php" class="_img">
                <img src="<?php echo $asset_path ?>imgs/activity/act-8.jpg"/>
              </a>
              <div class="_content">
                <p class="__title"><a href="activity-content.php">โครงการประกอบการพบนักศึกษา / คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง</a></p>
                <p class="__desc">คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง
                  เข้าร่วมโครงการสถานประกอบการพบนักศึกษา ณ คณะวิศวกรรมศาสตร์</p>
                <p class="__link"><a href="#">อ่านต่อ</a></p>
              </div>
            </div>
          </div>
        </div>

        <?php include('components/pagination.php') ?>

      </div>

    </div>
  </div>
</div>

<script>
  $(function () {
    $('#theForm').validate();
  });
</script>

<?php require_once("components/footer.php"); ?>
