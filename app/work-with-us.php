<?php
$LOGGED_IN = true;
require_once("components/header.php");
require_once ("components/social_share.php");
?>
<div class="ml-page page-wws">
  <div class="sec-banner"></div>
  <div class="layout-outer">
    <div class="layout-inner">
      <ul class="breadcrumb">
        <li><a href="index.php">หน้าแรก</a></li>
        <li><a class="active">ร่วมงานกับมาลี</a></li>
      </ul>
      <h1 class="heading1 centered primary">ร่วมงานกับมาลี</h1>
      <h3 class="heading3 centered primary">เราพร้อมตอบรับทุกความต้องการของชีวิต เพื่อให้คุณได้ใช้ชีวิตในแบบที่คุณเลือก และเติบโตไปพร้อมกับเรา</h3>

      <!-- The table with pagination -->
      <?php
      require_once('components/job_filter_table.php');
      get_job_filter_table(true)
      ?>

      <!-- Job type -->
      <div class="job-categories">
        <h1>ค้นหางานตามกลุ่มงาน</h1>

        <!-- Desktop -->
        <div class="jc-wrap desktop">
          <?php
          $cat_names = [
            'Accounting',
            'Demand & Supply Planning',
            'Customer Service',
            'Engineering',
            'Legal',
            'Management',
            'Export Import Shipping',
          ];
          for ($i = 0; $i < 8 * 3; $i++) { ?>
            <a href="#" class="__col">
              <div class="__title"><?php echo $cat_names[mt_rand(0, count($cat_names) - 1)] ?></div>
              <div class="__count"><?php echo mt_rand(1, 15) ?></div>
            </a>
          <?php } ?>
        </div>

        <!-- Mobile -->
        <div class="jc-wrap mobile">
          <?php
          $cat_names = [
            'Accounting',
            'Demand & Supply Planning',
            'Customer Service',
            'Engineering',
            'Legal',
            'Management',
            'Export Import Shipping',
          ];
          for ($i = 0; $i < 8; $i++) { ?>
            <a href="#" class="__col">
              <div class="__title"><?php echo $cat_names[mt_rand(0, count($cat_names) - 1)] ?></div>
              <div class="__count"><?php echo mt_rand(1, 15) ?></div>
            </a>
          <?php } ?>

          <!-- Button mobile demo only-->
          <div class="btn">ดูเพิ่มเติม</div>
        </div>
      </div>



    </div>
  </div>
</div>

<script>
  $(function () {
    $('#theForm').validate();
  });
</script>

<?php require_once("components/footer.php"); ?>
