<?php
require_once("components/header.php");
require_once ("components/social_share.php");
?>
<div class="ml-page page-apply-job">
  <div class="sec-banner"></div>
  <div class="layout-outer">
    <div class="layout-inner">
      <ul class="breadcrumb">
        <li><a href="index.php">หน้าแรก</a></li>
        <li><a href="work-with-us.php">ร่วมงานกับมาลี</a></li>
        <li><a href="#">Web Developer</a></li>
        <li><a href="#" class="active">สมัครงาน</a></li>
      </ul>
      <h1 class="heading1 centered">สมัครงาน</h1>
      <div class="wws-content-wrap">

        <div class="profile-info">
          <div class="__left">
            <img src="<?php echo $asset_path ?>imgs/avatar-placeholder.jpg">
          </div>
          <div class="__right">
            <div class="ml-px2">
              <h1>Tharadorl Test (Dol)</h1>
              <div class="__row">
                <div class="__col __label">ตำแหน่งงานล่าสุด</div>
                <div class="__col __desc">Project Manager</div>
              </div>
              <div class="__row">
                <div class="__col __label">การศึกษา</div>
                <div class="__col __desc">ปริญญาตรี คณะวิศวกรรมศาสตร์ สาขาซอฟต์แวร์และความรู้ มหาวิทยาลัยเกษตรศาสตร์</div>
              </div>
              <div class="__row">
                <div class="__col __label">เบอร์ติดต่อ</div>
                <div class="__col __desc">095 777 7777</div>
              </div>
              <div class="__row">
                <div class="__col __label">อีเมล</div>
                <div class="__col __desc">mail@mail.com</div>
              </div>
            </div>
            <div class="ml-form">
              <form id="theForm" class="clearfix wws-form" action="./" method="POST">

                <div class="sm-col col-12 px2">
                  <p class="form-caption">
                    กรุณากรอกข้อมูลของท่านให้ถูกต้อง ครบถ้วนสมบูรณ์<br/>
                    กรุณากรอกข้อมูลที่มีเครื่องหมาย * ให้ครบถ้วน
                  </p>
                </div>
                <div class="sm-col col-12 px2">
                  <div class="form-item">
                    <label class="label" for="source">ทราบข่าวสารการรับสมัครจาก</label>
                    <select class="ml-input" name="source" required>
                      <?php include('components/select_options.php') ?>
                    </select>
                  </div>
                </div>
                <div class="sm-col col-12 px2">
                  <div class="form-item">
                    <label class="label" for="unnamed">ที่อื่นโปรดระบุ</label>
                    <input class="ml-input" name="unnamed" type="text">
                  </div>
                </div>
                <div class="sm-col col-12 px2">
                  <div class="form-item">
                    <label class="label" for="unnamed">ตำแหน่งที่สมัคร</label>
                    <input class="ml-input" name="unnamed" type="text">
                  </div>
                </div>
                <div class="sm-col col-12 px2">
                  <div class="form-item">
                    <label class="label" for="start-date">เริ่มงานได้วันที่</label>
                    <input class="ml-input" data-toggle="datepicker" name="start_date">
                    <div class="icon-wrap fa-wrap">
                      <i class="fas fa-calendar-alt"></i>
                    </div>
                  </div>
                </div>
                <div class="sm-col col-12 px2">
                  <div class="form-item">
                    <label class="label" for="unnamed">เงินเดือนปัจจุบัน</label>
                    <input class="ml-input" name="unnamed" type="text">
                  </div>
                </div>
                <div class="sm-col col-12 px2">
                  <div class="form-item">
                    <label class="label" for="unnamed">เงินเดือนที่ต้องการ</label>
                    <input class="ml-input" name="unnamed" type="text" required>
                  </div>
                </div>
                <div class="sm-col col-12 px2">
                  <label class="form-item form-item-checkbox">สามารถต่อรองเงินได้
                    <input name="can_negotiate" type="checkbox" checked="checked">
                    <span class="checkmark"></span>
                  </label>
                </div>
                <div class="sm-col col-12 px2">
                  <button type="submit" class="btn btn-submit">ยืนยัน</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $(function () {
    $('#theForm').validate();
  });
</script>

<?php require_once("components/footer.php"); ?>
