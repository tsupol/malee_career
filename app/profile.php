<?php require_once("components/header.php"); ?>
<div class="ml-page page-profile">
  <div class="sec-banner-2"></div>
  <div class="layout-outer">
    <div class="layout-inner">
      <div class="layout-profile-page">

        <?php
        require_once('components/profile_nav.php');
        get_profile_nav(1)
        ?>

        <div class="sec-content">
          <h1 class="heading1">ภาพรวมบัญชีผู้ใช้</h1>
          <!-- Card 1-->
          <div class="card-profile">
            <div class="cp-inner">
              <h1>ประวัติการสมัครงาน</h1>
              <div class="pp-job-list job-table">

                <?php for ($i = 0; $i < 3; $i++) { ?>
                  <div class="__row">
                    <a href="job-description.php" class="__col col-title">
                      <p class="__title"><span class="bold">Software Developer / Programmer</span></p>
                      <p class="__date">24/05/2561</p>
                    </a>
                    <div class="__col col-company">
                      <p class="__company">บริษัท มาลีเอ็นเตอร์ไพรส์ จํากัด</p>
                      <p class="__company-branch">สำนักงานรังสิต</p>
                    </div>
                    <div class="__col col-print">
                      <a class="btn-underline" href="#"><i class="fas fa-print"></i>พิมพ์ใบสมัคร</a>
                    </div>
                  </div>
                <?php } ?>

              </div>
            </div>
            <a href="profile-history" class="btn small cp-btn">ดูทั้งหมด</a>
          </div>
          <!-- Card 2-->
          <div class="card-profile">
            <div class="cp-inner">
              <h1>โปรไฟล์</h1>
              <!-- Same as `apply-job` page -->
              <div class="profile-info">
                <div class="__left">
                  <img src="<?php echo $asset_path ?>imgs/avatar-placeholder.jpg">
                </div>
                <div class="__right">
                  <div class="ml-px2">
                    <h1>Tharadorl Test (Dol)</h1>
                    <div class="__row">
                      <div class="__col __label">ตำแหน่งงานล่าสุด</div>
                      <div class="__col __desc bold">Project Manager</div>
                    </div>
                    <div class="__row">
                      <div class="__col __label">การศึกษา</div>
                      <div class="__col __desc bold">ปริญญาตรี คณะวิศวกรรมศาสตร์ สาขาซอฟต์แวร์และความรู้ มหาวิทยาลัยเกษตรศาสตร์</div>
                    </div>
                    <div class="__row">
                      <div class="__col __label">เบอร์ติดต่อ</div>
                      <div class="__col __desc bold">095 777 7777</div>
                    </div>
                    <div class="__row">
                      <div class="__col __label">อีเมล</div>
                      <div class="__col __desc bold">mail@mail.com</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <a href="profile-edit" class="btn small cp-btn">แก้ไขโปรไฟล์</a>
          </div>
          <!-- Card 3-->
          <div class="card-profile">
            <div class="cp-inner">
              <h1>ตำแหน่งงานแนะนำ</h1>
              <div class="pp-job-list job-table recommended">

                <?php for ($i = 0; $i < 3; $i++) { ?>
                  <div class="__row">
                    <a href="job-description.php" class="__col col-title">
                      <p class="__title"><span class="bold">Software Developer / Programmer</span></p>
                      <p class="__date">24/05/2561</p>
                    </a>
                    <div class="__col col-amount">
                      <p class="__count bold">1 อัตรา</p>
                    </div>
                    <div class="__col col-company">
                      <p class="__company">บริษัท มาลีเอ็นเตอร์ไพรส์ จํากัด</p>
                      <p class="__company-branch">สำนักงานรังสิต</p>
                    </div>
                    <div class="__col col-action col-apply">
                      <p><a href="apply-job.php">สมัครงาน</a></p>
                    </div>
                    <div class="__col col-action">
                      <p><a class="pop-share">แชร์</a></p>
                    </div>
                  </div>
                <?php } ?>

              </div>
            </div>
            <a href="work-with-us" class="btn small cp-btn">ดูทั้งหมด</a>
          </div>

        </div>
      </div>
      <div class="flex-center">

      </div>
    </div>
  </div>
</div>

<script>
  $(function () {
    $('#theForm').validate();
  });
</script>

<?php require_once("components/footer.php"); ?>
